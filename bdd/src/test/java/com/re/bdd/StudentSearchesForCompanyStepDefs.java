package com.re.bdd;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StudentSearchesForCompanyStepDefs {
    @And("Views the search company filter")
    public void viewsTheSearchCompanyFilter() {
    }

    @When("Student enters as company name {string} and presses the search button")
    public void studentEntersAsCompanyNameAndPressesTheSearchButton(String arg0) {
    }

    @Then("Company {string} will be displayed")
    public void companyWillBeDisplayed(String arg0) {
    }
}
