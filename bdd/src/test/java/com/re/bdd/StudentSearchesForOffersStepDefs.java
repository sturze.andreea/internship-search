package com.re.bdd;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StudentSearchesForOffersStepDefs {
    @And("Views the search internship filter")
    public void viewsTheSearchInternshipFilter() {
    }

    @When("Student selects paid offers filter option and presses search button")
    public void studentSelectsPaidOffersFilterOptionAndPressesSearchButton() {
    }

    @Then("Only internships that are paid will be displayed")
    public void onlyInternshipsThatArePaidWillBeDisplayed() {
    }
}
