package com.re.bdd;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ApplyForOfferStepDefs {
    @And("Has not already applied for the offer {string}")
    public void hasNotAlreadyAppliedForTheOffer(String arg0) {

    }

    @And("Views the internship offer {string}")
    public void viewsTheInternshipOffer(String arg0) {
    }

    @When("Students presses apply button and chooses the {string} doc")
    public void studentsPressesApplyButtonAndChoosesTheDoc(String arg0) {
    }

    @Then("Student application is registered")
    public void studentApplicationIsRegistered() {
    }

    @And("The application has {string} status")
    public void theApplicationHasStatus(String arg0) {
    }

    @And("User has already applied for the internship {string}")
    public void userHasAlreadyAppliedForTheInternship(String arg0) {
    }

    @When("Student views the internship offer {string}")
    public void studentViewsTheInternshipOffer(String arg0) {
    }

    @Then("The offer will show the {string} status of the current application")
    public void theOfferWillShowTheStatusOfTheCurrentApplication(String arg0) {
    }
}
