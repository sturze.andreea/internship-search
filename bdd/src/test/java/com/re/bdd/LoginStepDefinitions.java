package com.re.bdd;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginStepDefinitions {
    @Given("Username is {string}")
    public void usernameIs(String username) {

    }

    @And("Password is {string}")
    public void passwordIs(String password) {

    }
    @When("User presses login button")
    public void userPressesLoginButton() {

    }
    @Then("User is logged as student {string}")
    public void userIsLoggedAsStudent(String username) {
    }

    @Then("User is logged as company {string}")
    public void userIsLoggedAsCompany(String companyName) {

    }

    @Then("An error with the message {string} is shown")
    public void anErrorWithTheMessageIsShown(String arg0) {

    }

    @And("The user {string} is not logged into the system")
    public void theUserIsNotLoggedIntoTheSystem(String arg0) {

    }
}
