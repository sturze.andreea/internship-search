package com.re.bdd;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StudentUploadsDocsStepDefs {
    @Given("User logged as student")
    public void userLoggedAsStudent() {
    }

    @And("Views profile page")
    public void viewsProfilePage() {
    }

    @When("User selects a file with name {string} and presses upload button")
    public void userSelectsAFileWithNameAndPressesUploadButton(String arg0) {
    }

    @Then("File {string} is added to the student's docs")
    public void fileIsAddedToTheStudentSDocs(String arg0) {
    }
}
