package com.re.bdd;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StudentViewsApplicationsStepDefs {
    @When("Student views his own internship applications")
    public void studentViewsHisOwnInternshipApplications() {
    }

    @Then("No application is displayed")
    public void noApplicationIsDisplayed() {
    }

    @Then("The application for the internship {string} of company {string} is displayed")
    public void theApplicationForTheInternshipOfCompanyIsDisplayed(String arg0, String arg1) {
    }
}
