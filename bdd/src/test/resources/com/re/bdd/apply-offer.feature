Feature: Student applies for an internship offer
  Scenario: Student has not already applied and successfully applies for offer
    Given User logged as student
    And Views the internship offer "best_react_native_internship"
    And Has not already applied for the offer "best_react_native_internship"
    When Students presses apply button and chooses the "my_cv" doc
    Then Student application is registered
    And The application has "Awaiting for Review" status

  Scenario: Student has already applied for offer
    Given User logged as student
    And User has already applied for the internship "best_spring_boot_internship"
    When Student views the internship offer "best_spring_boot_internship"
    Then The offer will show the "Awaiting for Review" status of the current application