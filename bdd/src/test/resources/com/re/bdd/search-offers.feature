Feature: Student searches for internship offers
  Scenario: Search for just paid internship offers
    Given User logged as student
    And Views the search internship filter
    When Student selects paid offers filter option and presses search button
    Then Only internships that are paid will be displayed