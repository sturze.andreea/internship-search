Feature: Student view his own applications
  Scenario: Student has not applied to any internship
    Given User is logged as student "paul"
    When Student views his own internship applications
    Then No application is displayed

  Scenario: Student has applied for at least one offer
    Given User is logged as student "gigi"
    When Student views his own internship applications
    Then The application for the internship "best_springboot_internship" of company "telenav" is displayed