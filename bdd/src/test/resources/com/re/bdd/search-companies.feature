Feature: Student searches for a company
  Scenario: Student searches for company "telenav"
    Given User logged as student
    And Views the search company filter
    When Student enters as company name "telenav" and presses the search button
    Then Company "telenav" will be displayed