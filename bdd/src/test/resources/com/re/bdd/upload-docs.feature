Feature: Student uploads documents

  Scenario: Docs are uploaded
    Given User logged as student
    And Views profile page
    When User selects a file with name "my_cv" and presses upload button
    Then File "my_cv" is added to the student's docs