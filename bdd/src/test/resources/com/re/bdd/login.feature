Feature: Login

  Scenario: Successful student login
    Given Username is "paul"
    And Password is "password"
    When User presses login button
    Then User is logged as student "paul"

  Scenario: Successful company login
    Given Username is "telenav"
    And Password is "password"
    When User presses login button
    Then User is logged as company "telenav"

  Scenario: Username does not exist
    Given Username is "bad_username"
    When User presses login button
    Then An error with the message "An account with the username bad_username does not exist" is shown
    And The user "bad_username" is not logged into the system

  Scenario: Username exists, but password is not correct
    Given Username is "paul"
    And Password is "1234"
    When User presses login button
    Then An error with the message "Invalid password" is shown
    And The user "paul" is not logged into the system