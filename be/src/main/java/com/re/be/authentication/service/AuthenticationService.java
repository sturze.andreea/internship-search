package com.re.be.authentication.service;

import com.re.be.authentication.domain.model.AccountDetails;
import com.re.be.authentication.domain.repository.AccountDetailsRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final AccountDetailsRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public UserWasAuthenticatedResponse login(LoginRequest loginRequest) {
        if (!accountRepository.existsByEmail(loginRequest.getEmail())) {
            throw new UsernameNotFoundException(loginRequest.getEmail());
        }

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                loginRequest.getEmail(),
                loginRequest.getPassword()
        ));
        AccountDetails accountDetails = (AccountDetails) authentication.getPrincipal();
        return UserWasAuthenticatedResponse.builder()
                .id(accountDetails.getId().getId())
                .role(accountDetails.getRole().toString())
                .build();
    }


    public CredentialsWereCreatedResponse createCredentials(CreateCredentialsRequest credentialsRequest) {
        if (accountRepository.existsByEmail(credentialsRequest.getEmail())) {
            throw new UsernameAlreadyUsedException();
        }
        AccountDetails details = AccountDetails.createWithNewIdentity(credentialsRequest.getEmail(),
                passwordEncoder.encode(credentialsRequest.getPassword()),
                credentialsRequest.getRole());

        accountRepository.save(details);

        return CredentialsWereCreatedResponse.builder()
                .accountId(details.getId())
                .role(details.getRole())
                .build();
    }
}
