package com.re.be.authentication.domain.model;

public enum UserRole {
    STUDENT, COMPANY
}
