package com.re.be.authentication.service;

import com.re.be.authentication.domain.model.UserRole;
import com.re.be.common.domain.model.AccountId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CredentialsWereCreatedResponse {
    private UserRole role;
    private AccountId accountId;
}
