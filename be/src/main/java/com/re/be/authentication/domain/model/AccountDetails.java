package com.re.be.authentication.domain.model;

import com.re.be.common.domain.BusinessObject;
import com.re.be.common.domain.model.AccountId;
import jakarta.persistence.Entity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Entity
public class AccountDetails extends BusinessObject<AccountId> implements UserDetails {

    private String email;
    private String password;
    private UserRole role;

    public static AccountDetails createWithNewIdentity(String email, String password, UserRole role) {
        AccountDetails details = new AccountDetails();
        details.setEmail(email);
        details.setPassword(password);
        details.setRole(role);
        details.setId(new AccountId(UUID.randomUUID()));
        return details;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + role.toString()));
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    protected AccountDetails() {
    }

    private String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    private void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    private void setRole(UserRole role) {
        this.role = role;
    }
}
