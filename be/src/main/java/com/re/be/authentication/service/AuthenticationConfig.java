package com.re.be.authentication.service;

import com.re.be.authentication.domain.model.AccountDetails;
import com.re.be.authentication.domain.model.UserRole;
import com.re.be.authentication.domain.repository.AccountDetailsRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class AuthenticationConfig {

    private final AccountDetailsRepository accountRepository;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.httpBasic(Customizer.withDefaults())
            .sessionManagement((session) -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        http.csrf(AbstractHttpConfigurer::disable);

        http.cors(Customizer.withDefaults());

        internshipOffersAuthorization(http);
        internshipApplicationsAuthorization(http);
        documentsAuthorization(http);
        notificationAuthorization(http);
        loginAuthorization(http);
        registerAuthorization(http);

        return http.build();
    }

    private void internshipApplicationsAuthorization(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .requestMatchers(HttpMethod.GET, "/applications")
                .hasRole(UserRole.STUDENT.name())
                .and()
                .authorizeRequests()
                .requestMatchers(HttpMethod.POST, "/application")
                .hasRole(UserRole.STUDENT.name())
                .and()
                .authorizeRequests()
                .requestMatchers(HttpMethod.PUT, "/application");
    }

    private void internshipOffersAuthorization(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .requestMatchers(HttpMethod.POST, "/offer")
                .hasRole(UserRole.COMPANY.name())
                .and()
                .authorizeRequests()
                .requestMatchers(HttpMethod.GET, "/offers", "/offers?**", "/offer")
                .hasRole(UserRole.STUDENT.name());
    }

    private void notificationAuthorization(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .requestMatchers(HttpMethod.GET, "/notification", "/notifications")
                .hasRole(UserRole.STUDENT.name());
    }

    private void documentsAuthorization(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .requestMatchers(HttpMethod.POST, "/document")
                .hasRole(UserRole.STUDENT.name())
                .and()
                .authorizeRequests()
                .requestMatchers(HttpMethod.GET, "/document")
                .hasRole(UserRole.STUDENT.name());
    }

    private void loginAuthorization(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .requestMatchers(HttpMethod.POST,
                        "/account/login")
                .permitAll();
    }

    private void registerAuthorization(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .requestMatchers(HttpMethod.POST,
                        "/company/profile/register",
                        "/student/profile/register")
                .permitAll();
    }

    @Bean
    public UserDetailsService userDetailsService() {
       return username -> {
           AccountDetails accountDetails = accountRepository.findByEmail(username);
           if (accountDetails == null) {
               throw new UsernameNotFoundException(username);
           }
           return accountDetails;
       };
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration authConfig) throws Exception {
        return authConfig.getAuthenticationManager();
    }

    @Bean
    public DaoAuthenticationProvider authProvider(UserDetailsService userDetailsService, PasswordEncoder encoder) {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(encoder);
        return authProvider;
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(List.of("*"));
        configuration.setAllowedMethods(List.of("*"));
        configuration.setAllowedHeaders(List.of("*"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

}
