package com.re.be.authentication.service;

import com.re.be.authentication.domain.model.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateCredentialsRequest {
    private String email;
    private String password;
    private UserRole role;
}
