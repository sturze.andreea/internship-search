package com.re.be.authentication.domain.repository;

import com.re.be.authentication.domain.model.AccountDetails;
import com.re.be.common.domain.model.AccountId;
import org.springframework.data.repository.CrudRepository;

public interface AccountDetailsRepository extends CrudRepository<AccountDetails, AccountId> {

    AccountDetails findByEmail(String email);
    Boolean existsByEmail(String email);
}
