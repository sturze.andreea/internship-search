package com.re.be.profile.domain.repository;

import com.re.be.common.domain.model.CompanyId;
import com.re.be.profile.domain.model.CompanyProfile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyProfileRepository extends CrudRepository<CompanyProfile, CompanyId> {
}
