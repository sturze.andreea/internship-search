package com.re.be.profile.domain.repository;

import com.re.be.common.domain.model.StudentId;
import com.re.be.profile.domain.model.StudentProfile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentProfileRepository extends CrudRepository<StudentProfile, StudentId> {
}
