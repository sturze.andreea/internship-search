package com.re.be.profile.service;

import com.re.be.common.domain.model.StudentId;
import com.re.be.profile.domain.model.StudentProfile;
import com.re.be.profile.domain.repository.StudentProfileRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class StudentProfileService {
    private final StudentProfileRepository studentProfileRepository;


    public void createStudentProfile(CreateStudentProfileRequest request) {
        StudentId studentId = new StudentId(request.getStudentId());

        if (!studentProfileRepository.existsById(studentId)) {
            StudentProfile profile = new StudentProfile(studentId, request.getFirstName(), request.getLastName());
            studentProfileRepository.save(profile);
        }
    }

}
