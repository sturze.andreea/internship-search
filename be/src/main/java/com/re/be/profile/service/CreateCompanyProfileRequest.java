package com.re.be.profile.service;

import com.re.be.common.domain.model.Address;
import com.re.be.common.domain.model.CompanyId;
import jakarta.persistence.Embedded;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class CreateCompanyProfileRequest {
    private String name;
    private String companyId;
    @Embedded
    private Address location;
    private String description;
}
