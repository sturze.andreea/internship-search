package com.re.be.profile.service;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class CreateStudentProfileRequest {
    private UUID studentId;
    private String firstName;
    private String lastName;
}
