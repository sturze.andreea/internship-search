package com.re.be.profile.domain.model;

import com.re.be.common.domain.BusinessObject;
import com.re.be.common.domain.model.StudentId;
import jakarta.persistence.Entity;
import jakarta.persistence.IdClass;

@Entity
public class StudentProfile extends BusinessObject<StudentId> {
    private String firstName;
    private String lastName;

    public StudentProfile(StudentId studentId, String firstName, String lastName) {
        super.setId(studentId);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    protected StudentProfile() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
