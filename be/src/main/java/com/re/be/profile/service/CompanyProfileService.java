package com.re.be.profile.service;

import com.re.be.common.domain.model.CompanyId;
import com.re.be.profile.domain.model.CompanyProfile;
import com.re.be.profile.domain.repository.CompanyProfileRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class CompanyProfileService {
    private final CompanyProfileRepository companyProfileRepository;

    public void createProfileForCompanyWithId(CreateCompanyProfileRequest request) {
        CompanyId companyId = new CompanyId(request.getCompanyId());
        if (!companyProfileRepository.existsById(companyId)) {
            CompanyProfile profile = new CompanyProfile(companyId,
                    request.getName(),
                    request.getLocation(),
                    request.getDescription());
            companyProfileRepository.save(profile);
        }
    }
}
