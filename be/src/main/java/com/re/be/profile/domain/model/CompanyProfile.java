package com.re.be.profile.domain.model;


import com.re.be.common.domain.BusinessObject;
import com.re.be.common.domain.model.Address;
import com.re.be.common.domain.model.CompanyId;
import jakarta.persistence.*;

@Entity
public class CompanyProfile {
    @EmbeddedId
    private CompanyId id;
    private String name;

    @Embedded
    private Address location;
    private String description;

    public CompanyProfile(CompanyId companyId,
                          String name,
                          Address location,
                          String description) {
        this.setId(companyId);
        this.name = name;
        this.location = location;
        this.description = description;
    }

    protected CompanyProfile() {
    }

    public String getName() {
        return name;
    }

    public Address getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public CompanyId getId() {
        return id;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setLocation(Address location) {
        this.location = location;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    private void setId(CompanyId id) {
        this.id = id;
    }
}
