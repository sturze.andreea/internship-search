package com.re.be.restApi.internshipAppplication;

import com.re.be.authentication.domain.model.AccountDetails;
import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.documents.domain.model.ApplicationDocumentId;
import com.re.be.internshipApplication.documents.domain.model.DocumentWithDataDto;
import com.re.be.internshipApplication.documents.domain.model.DocumentWithoutDataDto;
import com.re.be.internshipApplication.documents.service.ApplicationDocumentService;
import com.re.be.internshipApplication.documents.service.CreateDocumentRequest;
import com.re.be.internshipApplication.documents.service.DocumentNameAlreadyExistsException;
import com.re.be.internshipApplication.documents.service.DocumentWasCreatedResponse;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@AllArgsConstructor
public class ApplicationDocumentController {
    private final ApplicationDocumentService documentService;

    @RequestMapping(value = "/document", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<DocumentWasCreatedResponse> uploadDocumentToStudentProfile(@RequestParam("document") MultipartFile document,
                                                                                     Authentication authentication)  {
        AccountDetails details = (AccountDetails) authentication.getPrincipal();

        try {
            CreateDocumentRequest request = CreateDocumentRequest.builder()
                    .studentId(new StudentId(details.getId().getId()))
                    .documentName(StringUtils.cleanPath(document.getOriginalFilename()))
                    .data(document.getBytes())
                    .build();
            return ResponseEntity.ok(documentService.createDocument(request));
        } catch (IOException | DocumentNameAlreadyExistsException e ) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/document/{id}")
    public ResponseEntity<Resource> downloadDocument(@PathVariable("id") ApplicationDocumentId documentId) {
        DocumentWithDataDto document = documentService.getDocumentWithDataById(documentId);

        if (document == null) {
            return ResponseEntity.notFound().build();
        }

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + document.getDocumentName());
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .headers(header)
                .body(new ByteArrayResource(document.getData()));
    }

    @GetMapping("/documents")
    public ResponseEntity<List<DocumentWithoutDataDto>> getDocumentNamesForApplicant(Authentication authentication) {
        AccountDetails details = (AccountDetails) authentication.getPrincipal();
        return ResponseEntity.ok(
                documentService.getAllDocumentWithoutDataForApplicant(new StudentId(details.getId().getId())));
    }

}
