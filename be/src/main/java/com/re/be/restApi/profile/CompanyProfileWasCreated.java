package com.re.be.restApi.profile;

import com.re.be.authentication.domain.model.UserRole;
import com.re.be.common.domain.model.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompanyProfileWasCreated {
    private UUID companyId;
    private String email;
    private String name;
    private Address location;
    private String description;
    private UserRole role;
}
