package com.re.be.restApi.internshipAppplication;

import java.util.*;
import java.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.re.be.authentication.domain.model.AccountDetails;
import com.re.be.common.domain.model.CompanyId;
import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.offers.domain.model.InternshipOfferDTO;
import com.re.be.internshipApplication.offers.domain.model.InternshipOfferId;
import com.re.be.internshipApplication.offers.domain.model.InternshipOfferViewDto;
import com.re.be.internshipApplication.offers.service.InternshipOfferService;
import com.re.be.internshipApplication.offers.service.InternshipOfferWasPublishedResponse;
import com.re.be.internshipApplication.offers.service.PublishInternshipOfferRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@AllArgsConstructor
public class InternshipOfferController {

    private final InternshipOfferService offerService;

    @PostMapping("/offer")
    public ResponseEntity<InternshipOfferWasPublishedResponse> publishInternshipOffer(@RequestBody PublishInternshipOfferRequest request,
                                                                                        Authentication authentication) {
        AccountDetails details = (AccountDetails) authentication.getPrincipal();
        request.setPublishedBy(new CompanyId(details.getId().getString()));
        return ResponseEntity.ok(offerService.publishInternshipOffer(request));
    }

    @GetMapping("/offers")
    public ResponseEntity<List<InternshipOfferDTO>> getInternshipOffers(@RequestParam(required = false) String filter,
                                                                        @RequestParam(required = false) String domain,
                                                                        @RequestParam(required = false) String location,
                                                                        @RequestParam(required = false) Integer duration,
                                                                        @RequestParam(required = false)
                                                                        @JsonDeserialize(using = LocalDateDeserializer.class)
                                                                                    LocalDate startsAfter,
                                                                        @RequestParam(required = false) String paid
    ) {
        return ResponseEntity.ok(offerService.getInternshipOffers(filter,
                domain,
                location,
                duration,
                startsAfter,
                paid));
    }

    @GetMapping("/offer/{id}")
    public ResponseEntity<InternshipOfferViewDto> getOneInternshipOffer(@PathVariable UUID id, Authentication authentication) {
        AccountDetails accountDetails = (AccountDetails) authentication.getPrincipal();
        StudentId studentId = new StudentId(accountDetails.getId().getId());
        InternshipOfferViewDto dto = offerService.getInternshipOfferById(new InternshipOfferId(id), studentId);

        if (dto == null) {
            return ResponseEntity.notFound().build();
        }
        
        return ResponseEntity.ok(dto);
    }
}
