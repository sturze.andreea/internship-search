package com.re.be.restApi.internshipAppplication;

import com.re.be.authentication.domain.model.AccountDetails;
import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplication;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationListViewDto;
import com.re.be.internshipApplication.applications.service.*;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@AllArgsConstructor
public class InternshipApplicationController {
    private final InternshipApplicationService internshipApplicationService;

    @PostMapping("/application")
    public ResponseEntity<InternshipApplicationWasSentResponse> sendInternshipApplication(@RequestBody SendInternshipApplicationRequest request, Authentication authentication) {
        AccountDetails details = (AccountDetails) authentication.getPrincipal();
        request.setApplicantId(new StudentId(details.getId().getString()));
        return ResponseEntity.ok(internshipApplicationService.sendInternshipApplication(request));
    }

    @GetMapping("/applications")
    public ResponseEntity<List<InternshipApplicationListViewDto>> getInternshipApplicationsForStudent(Authentication authentication) {
        AccountDetails details = (AccountDetails) authentication.getPrincipal();
        return ResponseEntity.ok(internshipApplicationService.getInternshipApplicationsForStudent(new StudentId(details.getId().getString())));
    }

    @PutMapping("/application")
    public ResponseEntity<InternshipApplicationStatusChangedResponse> changeInternshipApplicationStatus(
            @RequestBody InternshipApplicationChangeStatusRequest request) {
        return ResponseEntity.ok(internshipApplicationService.changeInternshipApplicationStatus(request));
    }
}
