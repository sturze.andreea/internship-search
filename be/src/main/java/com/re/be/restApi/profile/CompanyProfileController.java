package com.re.be.restApi.profile;

import com.re.be.authentication.domain.model.UserRole;
import com.re.be.authentication.service.*;
import com.re.be.profile.service.CompanyProfileService;
import com.re.be.profile.service.CreateCompanyProfileRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/company/profile")
public class CompanyProfileController {
    private final AuthenticationService authenticationService;
    private final CompanyProfileService companyProfileService;

    @PostMapping("/register")
    @Transactional
    public ResponseEntity<CompanyProfileWasCreated> createCompanyProfile(
            @RequestBody
            CreateCompanyProfileWithCredentialsRequest request
    ) {
        CredentialsWereCreatedResponse credentialsResponse = null;
        try {
            credentialsResponse = authenticationService.createCredentials(
                    CreateCredentialsRequest.builder()
                            .email(request.getEmail())
                            .password(request.getPassword())
                            .role(UserRole.COMPANY)
                            .build()
            );
        } catch (UsernameAlreadyUsedException e) {
            return ResponseEntity.badRequest().build();

        }

        companyProfileService.createProfileForCompanyWithId(CreateCompanyProfileRequest.builder()
                        .companyId(credentialsResponse.getAccountId().getString())
                        .name(request.getName())
                        .description(request.getDescription())
                        .location(request.getLocation())
                        .build()
        );

        return ResponseEntity.ok(
                CompanyProfileWasCreated
                        .builder()
                        .companyId((credentialsResponse.getAccountId().getId()))
                        .name(request.getName())
                        .description(request.getDescription())
                        .location(request.getLocation())
                        .role(UserRole.COMPANY)
                        .email(request.getEmail())
                        .build()
        );
    }

}
