package com.re.be.restApi.profile;

import com.re.be.common.domain.model.Address;
import jakarta.persistence.Embedded;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateCompanyProfileWithCredentialsRequest {
    private String email;
    private String password;
    private String name;
    private Address location;
    private String description;
}
