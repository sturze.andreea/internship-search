package com.re.be.restApi.authentication;

import com.re.be.authentication.service.*;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
@AllArgsConstructor
public class AuthenticationController {
    private final AuthenticationService authenticationService;

    @PostMapping(path = "/login")
    public ResponseEntity<UserWasAuthenticatedResponse> login(@RequestBody LoginRequest request) {
        try {
            return ResponseEntity.ok(authenticationService.login(request));
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(404).build();
        } catch (BadCredentialsException e) {
            return ResponseEntity.status(401).build();
        }

    }

    @PostMapping(path = "/register")
    public ResponseEntity<CredentialsWereCreatedResponse> register(@RequestBody CreateCredentialsRequest request) {
        return ResponseEntity.ok(authenticationService.createCredentials(request));
    }

}
