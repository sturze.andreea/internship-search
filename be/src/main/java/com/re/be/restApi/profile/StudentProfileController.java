package com.re.be.restApi.profile;

import com.re.be.authentication.domain.model.UserRole;
import com.re.be.authentication.service.AuthenticationService;
import com.re.be.authentication.service.CreateCredentialsRequest;
import com.re.be.authentication.service.CredentialsWereCreatedResponse;
import com.re.be.authentication.service.UsernameAlreadyUsedException;
import com.re.be.profile.service.CreateStudentProfileRequest;
import com.re.be.profile.service.StudentProfileService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student/profile")
@AllArgsConstructor
public class StudentProfileController {
    private final AuthenticationService authenticationService;
    private final StudentProfileService studentProfileService;

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public ResponseEntity<?> createStudentProfile(@RequestBody CreateStudentProfileWithCredentialsRequest request) {
        CredentialsWereCreatedResponse credentialsResponse = null;

        try {
            credentialsResponse = authenticationService.createCredentials(CreateCredentialsRequest.builder()
                    .role(UserRole.STUDENT)
                    .email(request.getEmail())
                    .password(request.getPassword())
                    .build());

        } catch (UsernameAlreadyUsedException e) {
            return ResponseEntity.badRequest().build();
        }

        studentProfileService.createStudentProfile(CreateStudentProfileRequest.builder()
                        .studentId(credentialsResponse.getAccountId().getId())
                        .firstName(request.getFirstName())
                        .lastName(request.getLastName())
                .build());

        return ResponseEntity.ok(
            StudentProfileWasCreated.builder()
                    .userRole(UserRole.STUDENT)
                    .firstName(request.getFirstName())
                    .lastName(request.getLastName())
                    .email(request.getEmail())
                    .studentId(credentialsResponse.getAccountId().getId())
                    .build()
        );
    }

}
