package com.re.be.restApi.internshipAppplication;

import com.re.be.authentication.domain.model.AccountDetails;
import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.applicationNotifications.domain.model.InternshipApplicationNotificationId;
import com.re.be.internshipApplication.applicationNotifications.domain.model.NotificationDto;
import com.re.be.internshipApplication.applicationNotifications.service.InternshipApplicationNotificationService;
import com.re.be.internshipApplication.applicationNotifications.service.NotificationNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class InternshipApplicationNotificationController {
    private final InternshipApplicationNotificationService notificationService;

    @GetMapping("/notification/{id}")
    public ResponseEntity<NotificationDto> readNotification(@PathVariable InternshipApplicationNotificationId id,
                                                            Authentication authentication) {
        AccountDetails accountDetails = (AccountDetails) authentication.getPrincipal();
        StudentId studentId = new StudentId(accountDetails.getId().getId());

        try {
            return ResponseEntity.ok(notificationService.readNotificationById(id, studentId));
        } catch (NotificationNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/notifications")
    public ResponseEntity<List<NotificationDto>> getNotificationsReceivedBy(Authentication authentication) {
        AccountDetails accountDetails = (AccountDetails) authentication.getPrincipal();
        StudentId studentId = new StudentId(accountDetails.getId().getId());

        return ResponseEntity.ok(notificationService.findNotificationsReceivedBy(studentId));
    }
}
