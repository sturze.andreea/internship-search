package com.re.be.restApi.profile;

import com.re.be.authentication.domain.model.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentProfileWasCreated {
    private UUID studentId;
    private String firstName;
    private String lastName;
    private String email;
    private UserRole userRole;
}
