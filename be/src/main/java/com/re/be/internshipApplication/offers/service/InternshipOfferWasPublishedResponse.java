package com.re.be.internshipApplication.offers.service;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.re.be.common.domain.model.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InternshipOfferWasPublishedResponse implements Serializable {
    private UUID publishedBy;
    private UUID id;
    private String name;
    private String description;
    private String domainOfWork;
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate startsOn;
    private Integer durationInMonths;
    private Address location;
    private Set<String> requiredDocuments;
    private Set<String> requiredSkills;
    private Boolean paid;
    private BigDecimal salary;
}
