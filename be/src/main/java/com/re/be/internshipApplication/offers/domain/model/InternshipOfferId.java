package com.re.be.internshipApplication.offers.domain.model;

import com.re.be.common.domain.Identity;
import jakarta.persistence.Column;

import java.util.UUID;

public class InternshipOfferId implements Identity {

    @Column(name = "internship_offer_id")
    private UUID id;

    public InternshipOfferId(UUID id) {
        this.id = id;
    }

    public InternshipOfferId(String id) {
        this.id = UUID.fromString(id);
    }

    @Override
    public String getString() {
        return getId().toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InternshipOfferId)) return false;

        InternshipOfferId that = (InternshipOfferId) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    protected InternshipOfferId() {
    }

    public UUID getId() {
        return id;
    }

    private void setId(UUID id) {
        this.id = id;
    }


}
