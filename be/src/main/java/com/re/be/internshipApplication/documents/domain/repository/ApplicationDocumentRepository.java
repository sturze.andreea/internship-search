package com.re.be.internshipApplication.documents.domain.repository;

import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.documents.domain.model.ApplicationDocument;
import com.re.be.internshipApplication.documents.domain.model.ApplicationDocumentId;
import com.re.be.internshipApplication.documents.domain.model.DocumentWithDataDto;
import com.re.be.internshipApplication.documents.domain.model.DocumentWithoutDataDto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ApplicationDocumentRepository extends CrudRepository<ApplicationDocument, ApplicationDocumentId> {
    Boolean existsByDocumentNameAndAndApplicantId(String name, StudentId applicantId);

    @Query("""
        select new com.re.be.internshipApplication.documents.domain.model.DocumentWithDataDto(doc.id, doc.applicantId, doc.documentName, doc.data)
        from ApplicationDocument as doc
        where doc.id=:idParam
    """)
    DocumentWithDataDto findDocumentWithData(@Param("idParam") ApplicationDocumentId documentId);

    @Query("""
        select new com.re.be.internshipApplication.documents.domain.model.DocumentWithoutDataDto(doc.id, doc.documentName)
        from ApplicationDocument as doc
        where doc.applicantId=:studentIdParam
    """)
    List<DocumentWithoutDataDto> findAllDocumentsWithoutDataForApplicant(@Param("studentIdParam") StudentId studentId);
}
