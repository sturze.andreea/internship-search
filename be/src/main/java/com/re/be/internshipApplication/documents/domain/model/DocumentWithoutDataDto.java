package com.re.be.internshipApplication.documents.domain.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class DocumentWithoutDataDto implements Serializable {
    private UUID documentId;
    private String documentName;

    public DocumentWithoutDataDto(final ApplicationDocumentId documentId, final String documentName) {
        this.documentId = documentId.getId();
        this.documentName = documentName;
    }

    public UUID getDocumentId() {
        return documentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentId(UUID documentId) {
        this.documentId = documentId;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocumentWithoutDataDto that = (DocumentWithoutDataDto) o;
        return Objects.equals(documentId, that.documentId) && Objects.equals(documentName, that.documentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(documentId, documentName);
    }
}
