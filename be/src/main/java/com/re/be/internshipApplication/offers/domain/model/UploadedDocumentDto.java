package com.re.be.internshipApplication.offers.domain.model;

import lombok.*;

import java.io.Serializable;
import java.util.UUID;

public class UploadedDocumentDto implements Serializable {
    private UUID documentId;
    private String documentName;

    public UploadedDocumentDto(UUID documentId, String documentName) {
        this.documentId = documentId;
        this.documentName = documentName;
    }

    public UploadedDocumentDto() {
    }

    public UUID getDocumentId() {
        return documentId;
    }

    public void setDocumentId(UUID documentId) {
        this.documentId = documentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }
}
