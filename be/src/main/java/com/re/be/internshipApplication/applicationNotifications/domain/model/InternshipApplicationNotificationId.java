package com.re.be.internshipApplication.applicationNotifications.domain.model;

import com.re.be.common.domain.Identity;
import jakarta.persistence.Column;

import java.util.UUID;

public class InternshipApplicationNotificationId implements Identity {
    @Column(name = "internship_application_notification_id")
    private UUID id;

    public InternshipApplicationNotificationId(UUID id) {
        this.id = id;
    }

    public InternshipApplicationNotificationId(String id) {
        this.id = UUID.fromString(id);
    }

    public UUID getId() {
        return id;
    }

    @Override
    public String getString() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InternshipApplicationNotificationId)) return false;

        InternshipApplicationNotificationId that = (InternshipApplicationNotificationId) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    protected InternshipApplicationNotificationId() {
    }

    private void setId(UUID id) {
        this.id = id;
    }
}
