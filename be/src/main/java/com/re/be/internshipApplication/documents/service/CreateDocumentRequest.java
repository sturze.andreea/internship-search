package com.re.be.internshipApplication.documents.service;

import com.re.be.common.domain.model.StudentId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateDocumentRequest {
    private String documentName;
    private StudentId studentId;
    private byte[] data;
}
