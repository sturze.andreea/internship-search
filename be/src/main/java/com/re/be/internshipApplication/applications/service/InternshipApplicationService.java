package com.re.be.internshipApplication.applications.service;

import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.applicationNotifications.service.InternshipApplicationStatusListener;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplication;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationListViewDto;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationStatus;
import com.re.be.internshipApplication.applications.domain.repository.InternshipApplicationRepository;
import com.re.be.internshipApplication.documents.domain.model.ApplicationDocumentId;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class InternshipApplicationService {
    private final InternshipApplicationRepository internshipApplicationRepository;
    private final InternshipApplicationStatusListener applicationStatusListener;

    public InternshipApplicationWasSentResponse sendInternshipApplication(SendInternshipApplicationRequest request) {
        InternshipApplication application = InternshipApplication.createWithNewIdentity(
                request.getOfferId(),
                request.getApplicantId(),
                InternshipApplicationStatus.AWAITING_FOR_REVIEW,
                LocalDate.now(),
                LocalDate.now(),
                request.getDocumentIds()
        );
        internshipApplicationRepository.save(application);
        return InternshipApplicationWasSentResponse.builder()
                .id(application.getId().getId())
                .offerId(application.getOfferId().getId())
                .applicantId(application.getApplicantId().getId())
                .status(application.getStatus().toString())
                .appliedOn(application.getAppliedOn())
                .lastChangedOn(application.getLastChangedOn())
                .documentIds(application.getDocumentIds())
                .build();
    }

    public List<InternshipApplicationListViewDto> getInternshipApplicationsForStudent(StudentId studentId) {
        return internshipApplicationRepository.findAllInternshipApplicationsForStudent(studentId);
    }

    @Transactional
    public InternshipApplicationStatusChangedResponse changeInternshipApplicationStatus(InternshipApplicationChangeStatusRequest request) {
        InternshipApplication application = internshipApplicationRepository.findById(request.getId()).get();
        internshipApplicationRepository.updateInternshipApplicationStatus(request.getId(), request.getNewStatus());
        applicationStatusListener.applicationStatusChanged(request.getId(), request.getNewStatus());
        return InternshipApplicationStatusChangedResponse.builder()
                .id(request.getId().getId())
                .oldStatus(application.getStatus().toString())
                .newStatus(request.getNewStatus().toString())
                .build();
    }
}
