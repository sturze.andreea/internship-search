package com.re.be.internshipApplication.offers.domain.repository;

import com.re.be.internshipApplication.offers.domain.model.InternshipOfferId;
import com.re.be.internshipApplication.offers.domain.model.InternshipOffer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InternshipOfferRepository extends CrudRepository<InternshipOffer, InternshipOfferId> {
}
