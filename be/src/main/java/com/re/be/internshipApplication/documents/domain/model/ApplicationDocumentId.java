package com.re.be.internshipApplication.documents.domain.model;

import com.re.be.common.domain.Identity;
import jakarta.persistence.Column;

import java.util.UUID;

public class ApplicationDocumentId implements Identity {
    @Column(name = "application_document_id")
    private UUID id;

    public ApplicationDocumentId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public String getString() {
        return null;
    }

    protected ApplicationDocumentId() {
    }

    private void setId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ApplicationDocumentId)) return false;

        ApplicationDocumentId that = (ApplicationDocumentId) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public ApplicationDocumentId(String id) {
        this.id = UUID.fromString(id);
    }
}
