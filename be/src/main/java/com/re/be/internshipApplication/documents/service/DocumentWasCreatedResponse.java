package com.re.be.internshipApplication.documents.service;

import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.documents.domain.model.ApplicationDocumentId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DocumentWasCreatedResponse {
    private UUID documentId;
    private UUID studentId;
    private String documentName;
}
