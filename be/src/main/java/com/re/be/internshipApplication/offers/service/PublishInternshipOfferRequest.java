package com.re.be.internshipApplication.offers.service;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.re.be.common.domain.model.Address;
import com.re.be.common.domain.model.CompanyId;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@AllArgsConstructor
@Data
public class PublishInternshipOfferRequest implements Serializable {
        private CompanyId publishedBy;
        private String name;
        private String description;
        private String domainOfWork;

        @JsonDeserialize(using = LocalDateDeserializer.class)
        private LocalDate startsOn;
        private Integer durationInMonths;
        private Address location;
        private Set<String> requiredDocuments;
        private Set<String> requiredSkills;
        private Boolean paid;
        private BigDecimal salary;
}
