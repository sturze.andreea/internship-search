package com.re.be.internshipApplication.applications.domain.repository;

import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplication;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationId;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationListViewDto;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationStatus;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InternshipApplicationRepository extends CrudRepository<InternshipApplication, InternshipApplicationId> {

    @Query("""
        select new com.re.be.internshipApplication.applications.domain.model.InternshipApplicationListViewDto(app.id, app.offerId ,off.name, app.status)
        from InternshipApplication as app
        inner join InternshipOffer as off
        on app.offerId = off.id
        where app.applicantId=:studentIdParam
    """)
    List<InternshipApplicationListViewDto> findAllInternshipApplicationsForStudent(@Param("studentIdParam") StudentId studentId);

    @Modifying
    @Query("""
        update InternshipApplication app set app.status=:newStatus where app.id=:id
    """)
    public void updateInternshipApplicationStatus(@Param("id") InternshipApplicationId id, @Param("newStatus") InternshipApplicationStatus status);
}
