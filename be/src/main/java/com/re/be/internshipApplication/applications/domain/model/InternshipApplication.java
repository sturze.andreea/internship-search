package com.re.be.internshipApplication.applications.domain.model;

import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.documents.domain.model.ApplicationDocumentId;
import com.re.be.internshipApplication.offers.domain.model.InternshipOfferId;
import jakarta.persistence.*;
import org.springframework.web.bind.annotation.Mapping;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "internship_application")
public class InternshipApplication {
    @EmbeddedId
    private InternshipApplicationId id;
    @Embedded
    private InternshipOfferId offerId;
    @Embedded
    private StudentId applicantId;
    @Enumerated(EnumType.ORDINAL)
    private InternshipApplicationStatus status;
    private LocalDate appliedOn;
    private LocalDate lastChangedOn;
    @ElementCollection
    private Set<UUID> documentIds;

    public static InternshipApplication createWithNewIdentity(
            final InternshipOfferId offerId,
            final StudentId applicantId,
            final InternshipApplicationStatus status,
            final LocalDate appliedOn,
            final LocalDate lastChangedOn,
            final Set<UUID> documentIds
    ) {
        InternshipApplicationId internshipApplicationId = new InternshipApplicationId(UUID.randomUUID());
        InternshipApplication internshipApplication = new InternshipApplication();

        internshipApplication.setId(internshipApplicationId);
        internshipApplication.setOfferId(offerId);
        internshipApplication.setApplicantId(applicantId);
        internshipApplication.setStatus(status);
        internshipApplication.setAppliedOn(appliedOn);
        internshipApplication.setLastChangedOn(lastChangedOn);
        internshipApplication.setDocumentIds(documentIds);
        return internshipApplication;
    }

    public InternshipApplicationId getId() {
        return id;
    }

    public InternshipOfferId getOfferId() {
        return offerId;
    }

    public StudentId getApplicantId() {
        return applicantId;
    }

    public InternshipApplicationStatus getStatus() {
        return status;
    }

    public LocalDate getAppliedOn() {
        return appliedOn;
    }

    public LocalDate getLastChangedOn() {
        return lastChangedOn;
    }

    public Set<UUID> getDocumentIds() {
        return documentIds;
    }

    protected InternshipApplication(){}

    private void setId(InternshipApplicationId id) {
        this.id = id;
    }

    private void setOfferId(InternshipOfferId offerId) {
        this.offerId = offerId;
    }

    private void setApplicantId(StudentId applicantId) {
        this.applicantId = applicantId;
    }

    private void setStatus(InternshipApplicationStatus status) {
        this.status = status;
    }

    private void setAppliedOn(LocalDate appliedOn) {
        this.appliedOn = appliedOn;
    }

    private void setLastChangedOn(LocalDate lastChangedOn) {
        this.lastChangedOn = lastChangedOn;
    }

    private void setDocumentIds(Set<UUID> documentIds) {
        this.documentIds = documentIds;
    }
}