package com.re.be.internshipApplication.offers.domain.model;

public class InternshipOfferDTO {

    private String id;
    private String name;
    private String description;
    private String domainOfWork;


    public InternshipOfferDTO() {
    }

    public InternshipOfferDTO(InternshipOfferId id, String name, String description, String domainOfWork) {
        this.id = id.getString();
        this.name = name;
        this.description = description;
        this.domainOfWork = domainOfWork;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDomainOfWork() {
        return domainOfWork;
    }

}