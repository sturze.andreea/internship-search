package com.re.be.internshipApplication.applicationNotifications.domain.model;

public enum NotificationStatus {
    UNREAD(2),
    READ(1);

    private final int priority;

    NotificationStatus(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }
}
