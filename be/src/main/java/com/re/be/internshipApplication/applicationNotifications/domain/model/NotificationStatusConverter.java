package com.re.be.internshipApplication.applicationNotifications.domain.model;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.util.stream.Stream;

@Converter(autoApply = true)
public class NotificationStatusConverter implements AttributeConverter<NotificationStatus, Integer> {
    @Override
    public Integer convertToDatabaseColumn(NotificationStatus attribute) {
        return attribute == null ? null : attribute.getPriority();
    }

    @Override
    public NotificationStatus convertToEntityAttribute(Integer dbData) {
        return dbData == null
                ? null
                : Stream.of(NotificationStatus.values())
                .filter(t -> t.getPriority() == dbData)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format("Priority with sortNum %s is not exist", dbData)));
    }
}
