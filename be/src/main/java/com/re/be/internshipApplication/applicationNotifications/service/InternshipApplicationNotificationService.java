package com.re.be.internshipApplication.applicationNotifications.service;

import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.applicationNotifications.domain.model.InternshipApplicationNotification;
import com.re.be.internshipApplication.applicationNotifications.domain.model.InternshipApplicationNotificationId;
import com.re.be.internshipApplication.applicationNotifications.domain.model.NotificationDto;
import com.re.be.internshipApplication.applicationNotifications.domain.repository.InternshipApplicationNotificationRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class InternshipApplicationNotificationService {
    private final InternshipApplicationNotificationRepository notificationRepository;


    public NotificationDto readNotificationById(InternshipApplicationNotificationId notificationId,
                                                StudentId studentReader) {
        readNotification(notificationId, studentReader);
        return notificationRepository.findNotificationDto(notificationId, studentReader);
    }

    @Transactional
    void readNotification(InternshipApplicationNotificationId notificationId, StudentId studentReader) {
        Optional<InternshipApplicationNotification> notificationOpt =
                notificationRepository.findNotificationWithReceiver(notificationId, studentReader);

        notificationOpt.ifPresent(notification -> {
            notification.read();
            notificationRepository.save(notification);
        });
        notificationOpt.orElseThrow(NotificationNotFoundException::new);

    }

    public List<NotificationDto> findNotificationsReceivedBy(StudentId receiverId) {
        return notificationRepository.findNotificationsWithReceiver(receiverId,
            Sort.by(
                    List.of(
                            Sort.Order.desc("notificationStatus"),
                            Sort.Order.desc("notificationTimestamp")
                    )));
    }
}
