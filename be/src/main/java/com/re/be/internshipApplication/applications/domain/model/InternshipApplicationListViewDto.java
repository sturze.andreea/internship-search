package com.re.be.internshipApplication.applications.domain.model;

import com.re.be.internshipApplication.offers.domain.model.InternshipOfferId;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class InternshipApplicationListViewDto implements Serializable {
    private UUID id;
    private UUID offerId;
    private String offerName;
    private String status;

    public InternshipApplicationListViewDto(InternshipApplicationId id, InternshipOfferId offerId, String offerName, InternshipApplicationStatus status) {
        this.id = id.getId();
        this.offerId = offerId.getId();
        this.offerName = offerName;
        this.status = status.toString();
    }

    public UUID getId() {
        return id;
    }

    public UUID getOfferId() {
        return offerId;
    }

    public String getOfferName() {
        return offerName;
    }

    public String getStatus() {
        return status;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setOfferId(UUID offerId) {
        this.offerId = offerId;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InternshipApplicationListViewDto that = (InternshipApplicationListViewDto) o;
        return Objects.equals(id, that.id) && Objects.equals(offerName, that.offerName) && Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, offerName, status);
    }
}
