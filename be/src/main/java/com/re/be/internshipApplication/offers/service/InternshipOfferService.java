package com.re.be.internshipApplication.offers.service;

import java.util.*;
import java.time.LocalDate;

import com.re.be.common.domain.model.Address;
import com.re.be.common.domain.model.CompanyId;
import com.re.be.common.domain.model.StudentId;
import com.re.be.common.domain.service.ResultSetTupleConverter;
import com.re.be.internshipApplication.offers.domain.model.*;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.Tuple;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import com.re.be.internshipApplication.offers.domain.repository.InternshipOfferRepository;
import lombok.AllArgsConstructor;
import org.hibernate.query.TupleTransformer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class InternshipOfferService {
    private final InternshipOfferRepository internshipOfferRepository;
    private final EntityManager entityManager;

    @Transactional
    public InternshipOfferWasPublishedResponse publishInternshipOffer(PublishInternshipOfferRequest request) {
        InternshipOffer offer = InternshipOffer.createWithNewIdentity(
                request.getPublishedBy(),
                request.getName(),
                request.getDescription(),
                request.getDomainOfWork(),
                request.getStartsOn(),
                request.getDurationInMonths(),
                request.getLocation(),
                request.getRequiredDocuments(),
                request.getRequiredSkills(),
                request.getPaid(),
                request.getSalary());

        internshipOfferRepository.save(offer);

        return InternshipOfferWasPublishedResponse.builder()
                .publishedBy(offer.getPublishedBy().getId())
                .id(offer.getId().getId())
                .name(offer.getName())
                .description(offer.getDescription())
                .domainOfWork(offer.getDomainOfWork())
                .startsOn(offer.getStartsOn())
                .durationInMonths(offer.getDurationInMonths())
                .location(offer.getLocation())
                .requiredDocuments(offer.getRequiredDocuments())
                .requiredSkills(offer.getRequiredSkills())
                .paid(offer.getPaid())
                .salary(offer.getSalary())
                .build();
    }

    public List<InternshipOfferDTO> getInternshipOffers(String filter,
                                                        String domain,
                                                        String location,
                                                        Integer duration,
                                                        LocalDate startsAfter,
                                                        String paid) {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<InternshipOfferDTO> cq = cb
                .createQuery(InternshipOfferDTO.class);
        Root<InternshipOffer> root = cq.from(InternshipOffer.class);

        cq.select(cb.construct(
                InternshipOfferDTO.class,
                root.get("id"),
                root.get("name"),
                root.get("description"),
                root.get("domainOfWork")
        ));

        List<Predicate> predicates = new ArrayList<>();
        if (filter != null) {
            predicates.add(cb.or(cb.like(root.get("name"), "%" + filter + "%"), cb.like(root.get("description"), "%" + filter + "%")));
        }
        if (domain != null) {
            predicates.add(cb.equal(root.get("domainOfWork"), domain));
        }
        if (location != null) {
            predicates.add(cb.or(cb.like(root.get("location").get("city"), "%" + location + "%"), cb.like(root.get("location").get("country"), "%" + location + "%")));
        }
        if (duration != null) {
            predicates.add(cb.equal(root.get("durationInMonths"), duration));
        }
        if (startsAfter != null) {
            predicates.add(cb.greaterThanOrEqualTo(root.get("startsOn"), startsAfter));
        }

        if (paid != null) {
            if (paid.equals("paid")) {
                predicates.add(cb.equal(root.get("paid"), true));
            } else if (paid.equals("unpaid")) {
                predicates.add(cb.equal(root.get("paid"), false));
            }
        }

        cq.where(cb.and(predicates.toArray(new Predicate[0])));
        TypedQuery<InternshipOfferDTO> q = entityManager.createQuery(cq);

        return q.getResultList();
    }

    @SuppressWarnings("unchecked")
    public InternshipOfferViewDto getInternshipOfferById(InternshipOfferId offerId, StudentId studentId) {
        try {
            return (InternshipOfferViewDto) entityManager
                    .createNativeQuery("""
                    select
                        offer.internship_offer_id as id,
                        offer.name as name,
                        offer.description as description,
                        offer.domain_of_work as domain_of_work,
                        offer.starts_on as starts_on,
                        offer.duration_in_months as duration_in_months,
                        offer.paid as paid,
                        offer.salary as salary,
                        offer.city as location_city,
                        offer.country as location_country,
                        company.name as published_by,
                        skills.required_skills as required_skill,
                        requiredDocs.required_documents as required_document,
                        application.status as application_status,
                        docs.application_document_id as document_id,
                        docs.document_name as document_name,
                        application.internship_application_id as application_id
                    from internship_offer offer
                    join company_profile company on company.company_id=offer.company_id
                    left join internship_offer_required_skills skills
                        on skills.internship_offer_internship_offer_id=offer.internship_offer_id
                    left join internship_offer_required_documents requiredDocs
                        on requiredDocs.internship_offer_internship_offer_id=offer.internship_offer_id
                    left join internship_application application
                        on application.internship_offer_id=offer.internship_offer_id
                            and application.student_id = :studentParam
                    left join internship_application_document_ids uploadedDocs
                        on uploadedDocs.internship_application_internship_application_id=application.internship_application_id
                    left join application_documents docs on docs.application_document_id=uploadedDocs.document_ids and
                        docs.student_id=:studentParam
              
                    where offer.internship_offer_id=:idParam
                """)
                    .unwrap(org.hibernate.query.Query.class)
                    .setParameter("idParam", offerId.getId())
                    .setParameter("studentParam", studentId.getId())
                    .setTupleTransformer(new InternshipOfferViewDtoTupleTransformer())
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}
