package com.re.be.internshipApplication.applications.service;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationStatus;
import com.re.be.internshipApplication.documents.domain.model.ApplicationDocumentId;
import com.re.be.internshipApplication.offers.domain.model.InternshipOfferId;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
public class SendInternshipApplicationRequest implements Serializable {
    private InternshipOfferId offerId;
    private StudentId applicantId;
    private Set<UUID> documentIds;
}
