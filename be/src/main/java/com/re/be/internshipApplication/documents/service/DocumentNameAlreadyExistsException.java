package com.re.be.internshipApplication.documents.service;

public class DocumentNameAlreadyExistsException extends RuntimeException {
    public DocumentNameAlreadyExistsException() {
    }

    public DocumentNameAlreadyExistsException(String message) {
        super(message);
    }

    public DocumentNameAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public DocumentNameAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public DocumentNameAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
