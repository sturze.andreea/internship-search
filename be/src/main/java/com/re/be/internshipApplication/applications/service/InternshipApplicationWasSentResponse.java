package com.re.be.internshipApplication.applications.service;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InternshipApplicationWasSentResponse {
    private UUID id;
    private UUID offerId;
    private UUID applicantId;
    private String status;
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate appliedOn;
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate lastChangedOn;
    private Set<UUID> documentIds;
}
