package com.re.be.internshipApplication.documents.service;

import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.documents.domain.model.ApplicationDocument;
import com.re.be.internshipApplication.documents.domain.model.ApplicationDocumentId;
import com.re.be.internshipApplication.documents.domain.model.DocumentWithoutDataDto;
import com.re.be.internshipApplication.documents.domain.repository.ApplicationDocumentRepository;
import com.re.be.internshipApplication.documents.domain.model.DocumentWithDataDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class ApplicationDocumentService {

    private final ApplicationDocumentRepository documentRepository;

    @Transactional
    public DocumentWasCreatedResponse createDocument(CreateDocumentRequest request) {
        if (documentRepository.existsByDocumentNameAndAndApplicantId(request.getDocumentName(),
                request.getStudentId())) {
            throw new DocumentNameAlreadyExistsException();
        }

        ApplicationDocument document = ApplicationDocument.createWithNewIdentity(
                request.getStudentId(),
                request.getDocumentName(),
                request.getData()
        );

        documentRepository.save(document);

        return DocumentWasCreatedResponse.builder()
                .documentId(document.getId().getId())
                .studentId(document.getApplicantId().getId())
                .documentName(document.getDocumentName())
                .build();
    }

    public DocumentWithDataDto getDocumentWithDataById(ApplicationDocumentId documentId) {
        return documentRepository.findDocumentWithData(documentId);
    }

    public List<DocumentWithoutDataDto> getAllDocumentWithoutDataForApplicant(StudentId studentId) {
        return documentRepository.findAllDocumentsWithoutDataForApplicant(studentId);
    }
}
