package com.re.be.internshipApplication.applicationNotifications.domain.repository;


import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.applicationNotifications.domain.model.InternshipApplicationNotification;
import com.re.be.internshipApplication.applicationNotifications.domain.model.InternshipApplicationNotificationId;
import com.re.be.internshipApplication.applicationNotifications.domain.model.NotificationDto;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InternshipApplicationNotificationRepository
        extends CrudRepository<InternshipApplicationNotification, InternshipApplicationNotificationId> {

    @Query("""
        select new com.re.be.internshipApplication.applicationNotifications.domain.model.NotificationDto(
            notification.notificationStatus,
            notification.applicationStatus,
            notification.notificationTimestamp,
            offer.name,
            company.name,
            notification.applicationId,
            notification.id)
        from InternshipApplicationNotification as notification
        join InternshipApplication application on application.id=notification.applicationId
        join InternshipOffer offer on offer.id=application.offerId
        join CompanyProfile company on company.id=offer.publishedBy
        where notification.id=:idParam and application.applicantId=:receiverId
        
    """)

    NotificationDto findNotificationDto(@Param("idParam") InternshipApplicationNotificationId notificationId,
                                        @Param("receiverId")StudentId receiverId);

    @Query("""
        select notification
        from InternshipApplicationNotification as notification
        join InternshipApplication application on application.id=notification.applicationId
        where notification.id=:notificationId and application.applicantId=:receiverId
    """)

    Optional<InternshipApplicationNotification>
    findNotificationWithReceiver(@Param("notificationId") InternshipApplicationNotificationId notificationId,
                                 @Param("receiverId")StudentId receiverId);

    @Query("""
     select new com.re.be.internshipApplication.applicationNotifications.domain.model.NotificationDto(
            notification.notificationStatus,
            notification.applicationStatus,
            notification.notificationTimestamp,
            offer.name,
            company.name,
            notification.applicationId,
            notification.id)
        from InternshipApplicationNotification as notification
        join InternshipApplication application on application.id=notification.applicationId
        join InternshipOffer offer on offer.id=application.offerId
        join CompanyProfile company on company.id=offer.publishedBy
        where application.applicantId=:receiverId
    """)

    List<NotificationDto> findNotificationsWithReceiver(@Param("receiverId")StudentId receiverId, Sort sort);

}
