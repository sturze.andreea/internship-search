package com.re.be.internshipApplication.applications.service;

import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationId;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class InternshipApplicationChangeStatusRequest implements Serializable {
    private InternshipApplicationId id;
    private InternshipApplicationStatus newStatus;
}
