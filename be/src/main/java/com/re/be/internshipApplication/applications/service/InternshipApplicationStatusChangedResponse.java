package com.re.be.internshipApplication.applications.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InternshipApplicationStatusChangedResponse {
    private UUID id;
    private String oldStatus;
    private String newStatus;
}
