package com.re.be.internshipApplication.applicationNotifications.domain.model;

import com.re.be.internshipApplication.applicationNotifications.domain.model.NotificationStatus;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationId;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationStatus;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

public class NotificationDto implements Serializable {
    private NotificationStatus notificationStatus;
    private InternshipApplicationStatus applicationStatus;
    private LocalDateTime notificationTimestamp;
    private String internshipOfferName;
    private String companyName;
    private UUID applicationId;
    private UUID notificationId;

    public NotificationDto(NotificationStatus notificationStatus,
                           InternshipApplicationStatus applicationStatus,
                           LocalDateTime notificationTimestamp,
                           String internshipOfferName,
                           String companyName,
                           InternshipApplicationId applicationId,
                           InternshipApplicationNotificationId notificationId) {
        this.notificationStatus = notificationStatus;
        this.applicationStatus = applicationStatus;
        this.notificationTimestamp = notificationTimestamp;
        this.internshipOfferName = internshipOfferName;
        this.companyName = companyName;
        this.applicationId = applicationId.getId();
        this.notificationId = notificationId.getId();
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public InternshipApplicationStatus getApplicationStatus() {
        return applicationStatus;
    }

    public LocalDateTime getNotificationTimestamp() {
        return notificationTimestamp;
    }

    public String getInternshipOfferName() {
        return internshipOfferName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public UUID getApplicationId() {
        return applicationId;
    }

    public UUID getNotificationId() {
        return notificationId;
    }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public void setApplicationStatus(InternshipApplicationStatus applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public void setNotificationTimestamp(LocalDateTime notificationTimestamp) {
        this.notificationTimestamp = notificationTimestamp;
    }

    public void setInternshipOfferName(String internshipOfferName) {
        this.internshipOfferName = internshipOfferName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setApplicationId(UUID applicationId) {
        this.applicationId = applicationId;
    }

    public void setNotificationId(UUID notificationId) {
        this.notificationId = notificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NotificationDto)) return false;

        NotificationDto that = (NotificationDto) o;

        if (notificationStatus != that.notificationStatus) return false;
        if (applicationStatus != that.applicationStatus) return false;
        if (notificationTimestamp != null ? !notificationTimestamp.equals(that.notificationTimestamp) : that.notificationTimestamp != null)
            return false;
        if (internshipOfferName != null ? !internshipOfferName.equals(that.internshipOfferName) : that.internshipOfferName != null)
            return false;
        if (companyName != null ? !companyName.equals(that.companyName) : that.companyName != null) return false;
        if (applicationId != null ? !applicationId.equals(that.applicationId) : that.applicationId != null)
            return false;
        return notificationId != null ? notificationId.equals(that.notificationId) : that.notificationId == null;
    }

    @Override
    public int hashCode() {
        int result = notificationStatus != null ? notificationStatus.hashCode() : 0;
        result = 31 * result + (applicationStatus != null ? applicationStatus.hashCode() : 0);
        result = 31 * result + (notificationTimestamp != null ? notificationTimestamp.hashCode() : 0);
        result = 31 * result + (internshipOfferName != null ? internshipOfferName.hashCode() : 0);
        result = 31 * result + (companyName != null ? companyName.hashCode() : 0);
        result = 31 * result + (applicationId != null ? applicationId.hashCode() : 0);
        result = 31 * result + (notificationId != null ? notificationId.hashCode() : 0);
        return result;
    }
}
