package com.re.be.internshipApplication.offers.domain.model;

import com.re.be.common.domain.model.Address;
import com.re.be.common.domain.service.ResultSetTupleConverter;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationStatus;

import java.util.*;

public class InternshipOfferViewDtoTupleTransformer
        implements org.hibernate.query.TupleTransformer<InternshipOfferViewDto> {

    private InternshipOfferViewDto offerDto;
    private final List<String> requiredSkills = new ArrayList<>();
    private final List<String> requiredDocuments = new ArrayList<>();
    private final Map<UUID, UploadedDocumentDto> uploadedDocumentDtoMap = new HashMap<>();
    private final List<UploadedDocumentDto> uploadedDocuments = new ArrayList<>();

    @Override
    public InternshipOfferViewDto transformTuple(Object[] tuple, String[] aliases) {
        ResultSetTupleConverter converter = new ResultSetTupleConverter(aliases, tuple);

        if (offerDto == null) {

            offerDto = InternshipOfferViewDto.builder()
                    .id(converter.getUUID("id"))
                    .name(converter.getString("name"))
                    .publishedBy(converter.getString("published_by"))
                    .description(converter.getString("description"))
                    .domainOfWork(converter.getString("domain_of_work"))
                    .startsOn(converter.getLocalDate("starts_on"))
                    .durationInMonths(converter.getInteger("duration_in_months"))
                    .paid(converter.getBoolean("paid"))
                    .salary(converter.getBigDecimal("salary"))
                    .location(new Address(converter.getString("location_country"),
                                          converter.getString("location_city")))
                    .requiredSkills(requiredSkills)
                    .requiredDocuments(requiredDocuments)
                    .status(
                        InternshipApplicationStatus.of(converter.getShort("application_status"))
                    )
                    .uploadedDocuments(uploadedDocuments)
                    .build();
        }

        String skill = converter.getString("required_skill");
        if (skill != null && !requiredSkills.contains(skill)) {
            requiredSkills.add(skill);
        }
        String document = converter.getString("required_document");
        if (document != null && !requiredDocuments.contains(document)) {
            requiredDocuments.add(document);
        }

        UUID docId = converter.getUUID("document_id");
        String documentName = converter.getString("document_name");
        if (docId != null && documentName != null) {
            if (!uploadedDocumentDtoMap.containsKey(docId)) {
                UploadedDocumentDto dto = new UploadedDocumentDto(docId, documentName);
                uploadedDocumentDtoMap.put(docId, dto);
                uploadedDocuments.add(dto);
            }
        }

        return offerDto;
    }
}
