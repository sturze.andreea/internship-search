package com.re.be.internshipApplication.applications.domain.model;

import com.re.be.common.domain.Identity;
import jakarta.persistence.Column;

import java.util.UUID;

public class InternshipApplicationId implements Identity {

    @Column(name = "internship_application_id")
    private UUID id;

    public InternshipApplicationId(UUID id) {
        this.id = id;
    }

    public InternshipApplicationId(String id) {
        this.id = UUID.fromString(id);
    }

    @Override
    public String getString() {
        return getId().toString();
    }

    public UUID getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InternshipApplicationId)) return false;

        InternshipApplicationId that = (InternshipApplicationId) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    protected InternshipApplicationId() {
    }

    private void setId(UUID id) {
        this.id = id;
    }
}
