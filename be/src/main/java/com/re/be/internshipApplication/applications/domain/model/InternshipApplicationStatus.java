package com.re.be.internshipApplication.applications.domain.model;

public enum InternshipApplicationStatus {
    AWAITING_FOR_REVIEW,
    UNDER_REVIEW,
    ACCEPTED,
    REJECTED;

    public static InternshipApplicationStatus of(Short value) {
        if (value == null) {
            return null;
        }
        return switch (value) {
            case 0 -> AWAITING_FOR_REVIEW;
            case 1 -> UNDER_REVIEW;
            case 2 -> ACCEPTED;
            case 3 -> REJECTED;
            default -> null;
        };
    }
}
