package com.re.be.internshipApplication.documents.domain.model;


import com.re.be.common.domain.BusinessObject;
import com.re.be.common.domain.model.StudentId;
import jakarta.persistence.*;

import java.util.UUID;

@Entity
@Table(name = "application_documents")
public class ApplicationDocument extends BusinessObject<ApplicationDocumentId> {

  @Embedded
  private StudentId applicantId;

  private String documentName;
  @Lob
  private byte[] data;

  public static ApplicationDocument createWithNewIdentity(StudentId owner,
                                                          String documentName,
                                                          byte[] data) {
    ApplicationDocument document = new ApplicationDocument();
    document.setId(new ApplicationDocumentId(UUID.randomUUID()));
    document.setDocumentName(documentName);
    document.setApplicantId(owner);
    document.setData(data);
    return document;
  }

  public StudentId getApplicantId() {
    return applicantId;
  }

  public String getDocumentName() {
    return documentName;
  }

  public byte[] getData() {
    return data;
  }

  protected ApplicationDocument() {
  }

  private void setApplicantId(StudentId applicantId) {
    this.applicantId = applicantId;
  }

  private void setDocumentName(String documentName) {
    this.documentName = documentName;
  }

  private void setData(byte[] data) {
    this.data = data;
  }
}
