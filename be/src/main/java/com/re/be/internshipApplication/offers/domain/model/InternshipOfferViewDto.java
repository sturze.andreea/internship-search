package com.re.be.internshipApplication.offers.domain.model;

import com.re.be.common.domain.model.Address;
import com.re.be.common.domain.model.CompanyId;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InternshipOfferViewDto {

    private UUID id;
    private String publishedBy;
    private String name;
    private String description;
    private String domainOfWork;
    private LocalDate startsOn;
    private Integer durationInMonths;
    private Boolean paid;
    private BigDecimal salary;
    private Address location;
    private List<String> requiredDocuments;
    private List<String> requiredSkills;
    private InternshipApplicationStatus status;
    private List<UploadedDocumentDto> uploadedDocuments;
}
