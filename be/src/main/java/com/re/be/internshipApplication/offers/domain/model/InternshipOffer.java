package com.re.be.internshipApplication.offers.domain.model;

import com.re.be.common.domain.BusinessObject;
import com.re.be.common.domain.model.Address;
import com.re.be.common.domain.model.CompanyId;
import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "internship_offer")
public class InternshipOffer  {

    @EmbeddedId
    private InternshipOfferId id;

    @Embedded
    private CompanyId publishedBy;
    private String name;
    private String description;
    private String domainOfWork;
    private LocalDate startsOn;
    private Integer durationInMonths;
    private Boolean paid;
    private BigDecimal salary;

    @Embedded
    private Address location;

    @ElementCollection
    private Set<String> requiredDocuments;

    @ElementCollection
    private Set<String> requiredSkills;

    public static InternshipOffer createWithNewIdentity(
            CompanyId publishedBy,
            String name,
            String description,
            String domainOfWork,
            LocalDate startsOn,
            Integer durationInMonths,
            Address location,
            Set<String> requiredDocuments,
            Set<String> requiredSkills,
            Boolean paid,
            BigDecimal salary
    ) {
        InternshipOfferId offerId = new InternshipOfferId(UUID.randomUUID());
        InternshipOffer offer = new InternshipOffer();

        offer.setPublishedBy(publishedBy);
        offer.setName(name);
        offer.setDescription(description);
        offer.setDomainOfWork(domainOfWork);
        offer.setStartsOn(startsOn);
        offer.setDurationInMonths(durationInMonths);
        offer.setLocation(location);
        offer.setRequiredDocuments(requiredDocuments);
        offer.setRequiredSkills(requiredSkills);
        offer.setId(offerId);
        offer.setPaid(paid);
        offer.setSalary(salary);
        return offer;
    }

    public CompanyId getPublishedBy() {
        return publishedBy;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDomainOfWork() {
        return domainOfWork;
    }

    public LocalDate getStartsOn() {
        return startsOn;
    }

    public Integer getDurationInMonths() {
        return durationInMonths;
    }

    public Address getLocation() {
        return location;
    }

    public Set<String> getRequiredDocuments() {
        return requiredDocuments;
    }

    public Set<String> getRequiredSkills() {
        return requiredSkills;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public Boolean getPaid() {
        return paid;
    }

    public InternshipOfferId getId() {
        return id;
    }

    protected InternshipOffer() {

    }

    private void setPublishedBy(CompanyId publishedBy) {
        this.publishedBy = publishedBy;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    private void setDomainOfWork(String domainOfWork) {
        this.domainOfWork = domainOfWork;
    }

    private void setStartsOn(LocalDate startsOn) {
        this.startsOn = startsOn;
    }

    private void setDurationInMonths(Integer durationInMonths) {
        this.durationInMonths = durationInMonths;
    }

    private void setPaid(Boolean paid) {
        this.paid = paid;
    }

    private void setLocation(Address location) {
        this.location = location;
    }

    private void setRequiredDocuments(Set<String> requiredDocuments) {
        this.requiredDocuments = requiredDocuments;
    }

    private void setRequiredSkills(Set<String> requiredSkills) {
        this.requiredSkills = requiredSkills;
    }

    private void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    private void setId(InternshipOfferId id) {this.id = id; }
}
