package com.re.be.internshipApplication.applicationNotifications.domain.model;

import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationId;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationStatus;
import jakarta.persistence.Embedded;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class InternshipApplicationNotification {

    @EmbeddedId
    private InternshipApplicationNotificationId id;

    @Embedded
    private InternshipApplicationId applicationId;

    private InternshipApplicationStatus applicationStatus;
    private NotificationStatus notificationStatus;
    private LocalDateTime notificationTimestamp;

    public static InternshipApplicationNotification createNewUnreadNotification(InternshipApplicationId applicationId,
                                                                                InternshipApplicationStatus status) {
        InternshipApplicationNotification notification = new InternshipApplicationNotification();
        notification.setId(new InternshipApplicationNotificationId(UUID.randomUUID()));
        notification.setApplicationStatus(status);
        notification.setNotificationTimestamp(LocalDateTime.now());
        notification.setNotificationStatus(NotificationStatus.UNREAD);
        notification.setApplicationId(applicationId);
        return notification;
    }

    public void read() {
        this.setNotificationStatus(NotificationStatus.READ);
    }

    public InternshipApplicationNotificationId getId() {
        return id;
    }

    public InternshipApplicationStatus getApplicationStatus() {
        return applicationStatus;
    }

    public LocalDateTime getNotificationTimestamp() {
        return notificationTimestamp;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public InternshipApplicationId getApplicationId() {
        return this.applicationId;
    }

    protected InternshipApplicationNotification(InternshipApplicationNotificationId id,
                                                InternshipApplicationStatus applicationStatus,
                                                LocalDateTime notificationTimestamp) {
        this.id = id;
        this.applicationStatus = applicationStatus;
        this.notificationTimestamp = notificationTimestamp;
    }

    protected InternshipApplicationNotification() {
    }

    private void setId(InternshipApplicationNotificationId id) {
        this.id = id;
    }

    private void setApplicationStatus(InternshipApplicationStatus applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    private void setNotificationTimestamp(LocalDateTime notificationTimestamp) {
        this.notificationTimestamp = notificationTimestamp;
    }

    private void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    private void setApplicationId(InternshipApplicationId applicationId) {
        this.applicationId = applicationId;
    }
}
