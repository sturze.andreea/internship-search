package com.re.be.internshipApplication.applicationNotifications.service;

import com.re.be.internshipApplication.applicationNotifications.domain.model.InternshipApplicationNotification;
import com.re.be.internshipApplication.applicationNotifications.domain.repository.InternshipApplicationNotificationRepository;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationId;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationStatus;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class InternshipApplicationStatusListener {
    private final InternshipApplicationNotificationRepository notificationRepository;

    public void applicationStatusChanged(InternshipApplicationId applicationId, InternshipApplicationStatus status) {
        InternshipApplicationNotification notification =
                InternshipApplicationNotification.createNewUnreadNotification(applicationId, status);
        notificationRepository.save(notification);
    }
}
