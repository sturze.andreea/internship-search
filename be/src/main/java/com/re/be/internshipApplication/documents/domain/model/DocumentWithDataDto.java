package com.re.be.internshipApplication.documents.domain.model;

import com.re.be.common.domain.model.StudentId;
import com.re.be.internshipApplication.documents.domain.model.ApplicationDocumentId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;

public class DocumentWithDataDto implements Serializable {
    private UUID documentId;
    private UUID studentId;
    private String documentName;
    private byte[] data;

    public DocumentWithDataDto(ApplicationDocumentId documentId, StudentId studentId, String documentName, byte[] data) {
        this.documentId = documentId.getId();
        this.studentId = studentId.getId();
        this.documentName = documentName;
        this.data = data;
    }

    public UUID getDocumentId() {
        return documentId;
    }

    public UUID getStudentId() {
        return studentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public byte[] getData() {
        return data;
    }

    public void setDocumentId(UUID documentId) {
        this.documentId = documentId;
    }

    public void setStudentId(UUID studentId) {
        this.studentId = studentId;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DocumentWithDataDto)) return false;

        DocumentWithDataDto that = (DocumentWithDataDto) o;

        return documentId != null ? documentId.equals(that.documentId) : that.documentId == null;
    }

    @Override
    public int hashCode() {
        return documentId != null ? documentId.hashCode() : 0;
    }
}
