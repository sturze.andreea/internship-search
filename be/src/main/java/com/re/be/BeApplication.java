package com.re.be;

import com.re.be.internshipApplication.applicationNotifications.service.InternshipApplicationStatusListener;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationId;
import com.re.be.internshipApplication.applications.domain.model.InternshipApplicationStatus;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.UUID;

@SpringBootApplication
@AllArgsConstructor
public class BeApplication implements CommandLineRunner {

    private final InternshipApplicationStatusListener listener;

    public static void main(String[] args) {
        SpringApplication.run(BeApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
