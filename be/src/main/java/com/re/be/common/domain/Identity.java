package com.re.be.common.domain;

import jakarta.persistence.Embeddable;
import jakarta.persistence.MappedSuperclass;

import java.io.Serializable;

@Embeddable
@MappedSuperclass
public interface Identity extends Serializable{

    String getString();
}
