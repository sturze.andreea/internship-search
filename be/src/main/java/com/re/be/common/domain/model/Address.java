package com.re.be.common.domain.model;

import com.re.be.common.domain.ValueObject;
import jakarta.persistence.Column;

public class Address implements ValueObject {

    @Column(name = "country")
    private String country;

    @Column(name = "city")
    private String city;


    public Address(String country, String city) {
        this.country = country;
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;

        Address address = (Address) o;

        if (country != null ? !country.equals(address.country) : address.country != null) return false;
        return city != null ? city.equals(address.city) : address.city == null;
    }

    @Override
    public int hashCode() {
        int result = country != null ? country.hashCode() : 0;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        return result;
    }

    protected Address() {
    }

    private void setCountry(String country) {
        this.country = country;
    }

    private void setCity(String city) {
        this.city = city;
    }
}
