package com.re.be.common.domain.model;

import com.re.be.common.domain.Identity;
import jakarta.persistence.Column;

import java.util.UUID;

public class AccountId implements Identity {
    @Column(name = "account_id")
    private UUID id;

    public AccountId(UUID id) {
        this.id = id;
    }

    @Override
    public String getString() {
        return getId().toString();
    }

    public UUID getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountId)) return false;

        AccountId accountId = (AccountId) o;

        return id != null ? id.equals(accountId.id) : accountId.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    private void setId(UUID id) {
        this.id = id;
    }

    protected AccountId() {
    }

    protected AccountId(String id) {
        this.id = UUID.fromString(id);
    }
}
