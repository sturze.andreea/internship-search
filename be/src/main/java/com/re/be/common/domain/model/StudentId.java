package com.re.be.common.domain.model;

import com.re.be.common.domain.Identity;
import jakarta.persistence.Column;

import java.util.UUID;

public class StudentId implements Identity {

    @Column(name = "student_id")
    private UUID id;

    public StudentId(UUID id) {
        this.id = id;
    }
    public StudentId(String id) {
        this.id = UUID.fromString(id);
    }

    @Override
    public String getString() {
        return id.toString();
    }

    public UUID getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StudentId)) return false;

        StudentId studentId = (StudentId) o;

        return id != null ? id.equals(studentId.id) : studentId.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    protected StudentId() {
    }

    protected void setId(UUID id) {
        this.id = id;
    }
}
