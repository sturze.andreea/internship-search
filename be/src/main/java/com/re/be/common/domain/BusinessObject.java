package com.re.be.common.domain;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BusinessObject<ID extends Identity> {
    @EmbeddedId
    ID id;

    public ID getId() {
        return id;
    }

    protected BusinessObject() {
    }

    protected void setId(ID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BusinessObject)) return false;

        BusinessObject<?> that = (BusinessObject<?>) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
