package com.re.be.common.domain;

import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public interface ValueObject extends Serializable {
}
