package com.re.be.common.domain.service;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ResultSetTupleConverter {
    private Map<String, Integer> aliasToIndexMap;
    private Object[] tuple;

    public ResultSetTupleConverter(String[] aliases, Object[] tuple) {
        this.aliasToIndexMap = aliasToIndexMap(aliases);
        this.tuple = tuple;
    }

    public <T> T getProperty(String name, Class<T> type) {
        return type.cast(tuple[aliasToIndexMap.get(name)]);
    }

    public String getString(String name) {
        return getProperty(name, String.class);
    }

    public BigInteger getBigInteger(String name) {
        return getProperty(name, BigInteger.class);
    }

    public Long getLong(String name) {
        return getProperty(name, Long.class);
    }

    public BigDecimal getBigDecimal(String name) {
        return getProperty(name, BigDecimal.class);
    }

    public Double getDouble(String name) {
        return getProperty(name, Double.class);
    }

    public LocalDate getLocalDate(String name) {
        try {
            Date sqlDate = getProperty(name, Date.class);
            return sqlDate.toLocalDate();
        } catch (ClassCastException e) {
            return getProperty(name, LocalDate.class);
        }
    }

    public Integer getInteger(String name) {
        return getProperty(name, Integer.class);
    }

    private Map<String, Integer> aliasToIndexMap(String[] aliases) {
        Map<String, Integer> aliasToIndexMap = new HashMap<>();

        for (int i = 0; i < aliases.length; i++) {
            aliasToIndexMap.put(aliases[i], i);
        }

        return aliasToIndexMap;
    }

    public Boolean getBoolean(String name) {
        return getProperty(name, Boolean.class);
    }

    public UUID getUUID(String name) {
        return getProperty(name, UUID.class);
    }

    public Short getShort(String name) {
        return getProperty(name, Short.class);
    }
}
