package com.re.be.common.domain.model;

import com.re.be.common.domain.Identity;
import jakarta.persistence.Column;

import java.util.UUID;

public class CompanyId  implements Identity{
    @Column(name = "company_id")
    private UUID id;

    public CompanyId(UUID id) {
        this.id = id;
    }
    public CompanyId(String id) {
        this.id = UUID.fromString(id);
    }

    @Override
    public String getString() {
        return id.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CompanyId)) return false;

        CompanyId companyId = (CompanyId) o;

        return id != null ? id.equals(companyId.id) : companyId.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    protected CompanyId() {
        super();
    }

    public UUID getId() {
        return id;
    }

    protected void setId(UUID id) {
        this.id = id;
    }
}
