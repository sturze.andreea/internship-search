import { Navigate } from "react-router";
import { UserRoles } from "../auth/components/UserRoles";

export const getRedirectBasedOnAuthRole = (auth) => {
  if (auth) {
    const role = auth.role;
    if (role) {
      if (role === UserRoles.COMPANY) {
        return <Navigate to={"/company/offer/publish"} />;
      } else if (role === UserRoles.STUDENT) {
        return <Navigate to={"/student/search"} />;
      }
    }
  }
};
