import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Dropdown,
  Form,
  Row,
} from "react-bootstrap";
import { CompanyToolbar } from "./CompanyToolbar";
import DatePicker from "react-datepicker";
import { useContext, useState } from "react";
import "react-datepicker/dist/react-datepicker.css";
import { PublishInternshipOfferApi } from "../api/PublishInternshipOfferApi";
import { AuthContext } from "../../auth/components/AuthContextProvider";
import { handleSuccess } from "../../utils/SuccessToast";
import { handleError } from "../../utils/ErrorToast";

const convertToLocalDateStr = (date) => {
  const indexOfT = date.toISOString().indexOf("T");
  return date.toISOString().substr(0, indexOfT);
};

export const PublishInternshipOfferComponent = () => {
  const INTERNSHIP_OFFER_INITIAL_STATE = {
    name: "",
    description: "",
    domainOfWork: "",
    startsOn: convertToLocalDateStr(new Date()),
    durationInMonths: "",
    location: {
      country: "",
      city: "",
    },
    requiredDocuments: [],
    requiredSkills: [],
    paid: false,
    salary: "",
  };

  const [internshipOfferState, setInternshipOfferState] = useState(
    INTERNSHIP_OFFER_INITIAL_STATE
  );
  const [selectedStartsOnDate, setSelectedStartsOnDate] = useState(new Date());
  const [isValidated, setIsValidated] = useState(false);

  const requiredDocumentsOptions = [
    "CV",
    "Cover Letter",
    "Language Certificate",
  ];
  const requiredSkillsOptions = ["Teamwork", "Programming", "Leader"];

  const { auth, logout } = useContext(AuthContext);

  const toggleSelectedRequiredDocuments = (option) => {
    if (internshipOfferState.requiredDocuments.includes(option)) {
      setInternshipOfferState({
        ...internshipOfferState,
        requiredDocuments: internshipOfferState.requiredDocuments.filter(
          (str) => str !== option
        ),
      });
    } else {
      setInternshipOfferState({
        ...internshipOfferState,
        requiredDocuments: [...internshipOfferState.requiredDocuments, option],
      });
      setHasProvidedDocs(true);
    }
  };

  const toggleSelectedRequiredSkills = (option) => {
    if (internshipOfferState.requiredSkills.includes(option)) {
      setInternshipOfferState({
        ...internshipOfferState,
        requiredSkills: internshipOfferState.requiredSkills.filter(
          (str) => str !== option
        ),
      });
    } else {
      setInternshipOfferState({
        ...internshipOfferState,
        requiredSkills: [...internshipOfferState.requiredSkills, option],
      });
    }
  };

  const handleNameChange = (e) => {
    e.preventDefault();
    setInternshipOfferState({ ...internshipOfferState, name: e.target.value });
  };

  const handleDescriptionChange = (e) => {
    e.preventDefault();
    setInternshipOfferState({
      ...internshipOfferState,
      description: e.target.value,
    });
  };

  const handleDomainOfWorkChange = (e) => {
    e.preventDefault();
    setInternshipOfferState({
      ...internshipOfferState,
      domainOfWork: e.target.value,
    });
  };

  const handleDurationInMonthsChange = (e) => {
    e.preventDefault();
    const value = e.target.value.replace(/\D/g, "");
    setInternshipOfferState({
      ...internshipOfferState,
      durationInMonths: value,
    });
  };

  const handleCountryLocationChange = (e) => {
    e.preventDefault();
    setInternshipOfferState({
      ...internshipOfferState,
      location: { ...internshipOfferState.location, country: e.target.value },
    });
  };

  const handleCityLocationChange = (e) => {
    e.preventDefault();
    setInternshipOfferState({
      ...internshipOfferState,
      location: { ...internshipOfferState.location, city: e.target.value },
    });
  };

  const selectedStartsOnDateChanged = (date) => {
    setSelectedStartsOnDate(date);
    setInternshipOfferState({
      ...internshipOfferState,
      startsOn: convertToLocalDateStr(date),
    });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    const apiRequest = { ...internshipOfferState };

    if (!internshipOfferState.paid) {
      apiRequest.salary = "";
    }

    if (
      apiRequest.requiredDocuments === null ||
      apiRequest.requiredDocuments.length === 0
    ) {
      setHasProvidedDocs(false);
      return;
    }

    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.preventDefault();
      e.stopPropagation();
      setIsValidated(true);
      if (apiRequest.requiredDocuments.length === 0) {
        setHasProvidedDocs(false);
      }
      return;
    }

    PublishInternshipOfferApi(apiRequest, auth)
      .catch((err) => {
        handleError("Internship offer could not be created");
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((r) => {
        if (r) {
          if (r.status === 200) {
            handleSuccess("Internship offer has been published");
          }
        }
      });
  };

  const handleClickOnPaidSwitch = (e) => {
    setInternshipOfferState({
      ...internshipOfferState,
      paid: !internshipOfferState.paid,
    });
  };

  const handleSalaryChange = (e) => {
    e.preventDefault();
    const value = e.target.value.replace(/\D/g, "");
    setInternshipOfferState({ ...internshipOfferState, salary: value });
  };

  const isSalaryValid = () => {
    return (
      internshipOfferState.salary !== "" &&
      Number(internshipOfferState.salary) > 0 &&
      internshipOfferState.paid
    );
  };

  const [hasProvidedDocs, setHasProvidedDocs] = useState(true);

  return (
    <Container fluid>
      <CompanyToolbar />

      <Row>
        <Col id="formTitle">
          <h2>Publish Internship Offer</h2>
        </Col>
      </Row>

      <Row>
        <Col className="col-md-6 mx-auto formContainer">
          <Card className="formCard">
            <CardBody className="formBody">
              <Form
                noValidate
                validated={isValidated}
                onSubmit={handleFormSubmit}
              >
                <Row className="formRow">
                  <Col className="col-md-10 mx-auto">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      type="text"
                      id="nameInput"
                      required
                      value={internshipOfferState.name}
                      onChange={handleNameChange}
                    />
                    <Form.Control.Feedback type="invalid">
                      Please provide company name
                    </Form.Control.Feedback>
                  </Col>
                </Row>
                <Row className="formRow">
                  <Col className="col-md-10 mx-auto">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      type="text"
                      id="descriptionInput"
                      required
                      value={internshipOfferState.description}
                      onChange={handleDescriptionChange}
                    />
                    <Form.Control.Feedback type="invalid">
                      Please provide description
                    </Form.Control.Feedback>
                  </Col>
                </Row>
                <Row className="formRow">
                  <Col className="col-md-10 mx-auto">
                    <Form.Label>Domain of work</Form.Label>
                    <Form.Control
                      type="text"
                      id="domainOfWorkInput"
                      required
                      value={internshipOfferState.domainOfWork}
                      onChange={handleDomainOfWorkChange}
                    />
                    <Form.Control.Feedback type="invalid">
                      Please provide domain of work
                    </Form.Control.Feedback>
                  </Col>
                </Row>

                <Row className="formRow">
                  <Col>
                    <Row>
                      <Col>
                        <Form.Label>Starts on</Form.Label>
                      </Col>
                    </Row>

                    <Row>
                      <Col>
                        <DatePicker
                          className="form-control"
                          selected={selectedStartsOnDate}
                          onChange={selectedStartsOnDateChanged}
                        />
                      </Col>
                    </Row>
                  </Col>
                  <Col>
                    <Form.Label>Duration (in months)</Form.Label>
                    <Form.Control
                      type="text"
                      id="durationInMonthsInput"
                      required
                      value={internshipOfferState.durationInMonths}
                      onChange={handleDurationInMonthsChange}
                    />
                    <Form.Control.Feedback type="invalid">
                      Please provide duration
                    </Form.Control.Feedback>
                  </Col>
                </Row>

                <Row className="formRow">
                  <Col>
                    <Form.Label>Country</Form.Label>
                    <Form.Control
                      type="text"
                      id="countryInput"
                      required
                      value={internshipOfferState.location.country}
                      onChange={handleCountryLocationChange}
                    />
                    <Form.Control.Feedback type="invalid">
                      Please provide country
                    </Form.Control.Feedback>
                  </Col>

                  <Col>
                    <Form.Label>City</Form.Label>
                    <Form.Control
                      type="text"
                      id="cityInput"
                      required
                      value={internshipOfferState.location.city}
                      onChange={handleCityLocationChange}
                    />
                    <Form.Control.Feedback type="invalid">
                      Please provide city
                    </Form.Control.Feedback>
                  </Col>
                </Row>

                <Row className="formRow">
                  <Col>
                    <Form.Label>Payment</Form.Label>
                    <Form.Check
                      type="switch"
                      id="paidSwitch"
                      label="Paid"
                      onClick={handleClickOnPaidSwitch}
                    />
                  </Col>
                  {internshipOfferState.paid && (
                    <Col>
                      <Form.Label>Salary</Form.Label>
                      <Form.Control
                        type="number"
                        id="salaryInput"
                        required
                        value={internshipOfferState.salary}
                        min={1}
                        onChange={handleSalaryChange}
                        isValid={isSalaryValid(internshipOfferState.salary)}
                      />
                      <Form.Control.Feedback type="invalid">
                        Please provide a non zero salary
                      </Form.Control.Feedback>
                    </Col>
                  )}
                </Row>

                <Row className="formRow">
                  <Col>
                    <Dropdown>
                      <Dropdown.Toggle variant="success" id="dropdown-basic">
                        Select required skills
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        {requiredSkillsOptions.map((option, index) => (
                          <Dropdown.Item
                            key={"requiredSkillsOption" + index}
                            onClick={() => toggleSelectedRequiredSkills(option)}
                            active={internshipOfferState.requiredSkills.includes(
                              option
                            )}
                          >
                            {option}
                          </Dropdown.Item>
                        ))}
                      </Dropdown.Menu>
                    </Dropdown>
                  </Col>
                  <Col>
                    <Row>
                      {internshipOfferState.requiredSkills.map(
                        (skill, index) => (
                          <Col
                            className="selectedOption"
                            key={"selectedRequiredSkills" + index}
                          >
                            {skill}
                          </Col>
                        )
                      )}
                    </Row>
                  </Col>
                </Row>
                <Row className="formRow">
                  <Col>
                    <Dropdown>
                      <Dropdown.Toggle variant="success" id="dropdown-basic">
                        Select required documents
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        {requiredDocumentsOptions.map((option, index) => (
                          <Dropdown.Item
                            key={"requiredDocumentsOption" + index}
                            onClick={() =>
                              toggleSelectedRequiredDocuments(option)
                            }
                            active={internshipOfferState.requiredDocuments.includes(
                              option
                            )}
                          >
                            {option}
                          </Dropdown.Item>
                        ))}
                      </Dropdown.Menu>
                    </Dropdown>
                    {!hasProvidedDocs && (
                      <div
                        style={{
                          width: "100%",
                          marginTop: "0.25rem",
                          fontSize: ".875em",
                          color: "var(--bs-form-invalid-color)",
                        }}
                      >
                        Please provide at least one document
                      </div>
                    )}
                  </Col>
                  <Col>
                    <Row>
                      {internshipOfferState.requiredDocuments.map(
                        (doc, index) => (
                          <Col
                            className="selectedOption"
                            key={"selectedRequiredDocuments" + index}
                          >
                            {doc}
                          </Col>
                        )
                      )}
                    </Row>
                  </Col>
                </Row>

                <Row className="formRow ">
                  <Col className="justify-content-center">
                    <Button id="publishOfferSubmit" type="submit">
                      Publish
                    </Button>
                  </Col>
                </Row>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
