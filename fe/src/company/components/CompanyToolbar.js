import { Col, Nav, NavDropdown, Row } from "react-bootstrap";
import { Person } from "react-bootstrap-icons";
import { useContext } from "react";
import { AuthContext } from "../../auth/components/AuthContextProvider";
import { Navigate, useNavigate } from "react-router-dom";
import logo from "../../logo.png";

export const CompanyToolbar = () => {
  const { logout, auth } = useContext(AuthContext);
  const navigate = useNavigate();

  if (!auth) {
    return <Navigate to={"/login"} />;
  }

  return (
    <Row id="pageToolbar">
      <Col className="d-flex flex-column justify-content-center">
        <img
          src={logo}
          alt="logo"
          width={40}
          onClick={() => {
            navigate("/company/offer/publish");
          }}
        />
      </Col>
      <Col>
        <Nav className="justify-content-end">
          <NavDropdown title={<Person />}>
            <NavDropdown.Item
              className="nav-link ToolbarLink"
              href="/company/offer/publish"
            >
              Publish Offer
            </NavDropdown.Item>
            <NavDropdown.Item
              className="nav-link ToolbarLink"
              onClick={() => logout()}
            >
              Logout
            </NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Col>
    </Row>
  );
};
