import axios from "axios";
import { GetAuthorizationHeader } from "../../auth/utils/GetAuthorizationHeader";

export const PublishInternshipOfferApi = (offer, auth) => {
  return axios.post(
    "http://localhost:8080/be/api/offer",
    offer,
    GetAuthorizationHeader(auth)
  );
};
