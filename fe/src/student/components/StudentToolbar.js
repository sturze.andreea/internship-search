import { Col, Nav, NavDropdown, Row } from "react-bootstrap";
import { Bell, Person } from "react-bootstrap-icons";
import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../auth/components/AuthContextProvider";
import { Navigate, useNavigate } from "react-router-dom";
import logo from "../../logo.png";
import {
  GetNotificationsApi,
  NotificationStatuses,
  ReadNotificationApi,
} from "../api/GetNotifications";
import { ApplicationStatusesToString } from "../api/GetInternshipOffer";
import { DateTime } from "luxon";

export const StudentToolbar = () => {
  const { auth, logout } = useContext(AuthContext);
  const navigate = useNavigate();
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    GetNotificationsApi(auth)
      .catch((err) => {
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((r) => {
        if (r) {
          if (r.status === 200) {
            setNotifications(
              r.data.sort((a, b) => {
                return (
                  new Date(b.notificationTimestamp) -
                  new Date(a.notificationTimestamp)
                );
              })
            );
          }
        }
      });
  }, [auth, logout]);

  const readNotification = (notification) => {
    ReadNotificationApi(notification.notificationId, auth)
      .catch((err) => {
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((r) => {
        if (r) {
          if (r.status === 200) {
            setNotifications(
              notifications.map((notif) => {
                if (notif.notificationId === r.data.notificationId)
                  return r.data;
                return notif;
              })
            );
          }
        }
      });
  };
  if (!auth) {
    return <Navigate to={"/login"} />;
  }

  return (
    <Row id="pageToolbar">
      <Col className="d-flex flex-column justify-content-center">
        <img
          src={logo}
          alt="logo"
          width={40}
          onClick={() => {
            navigate("/student/search");
          }}
        />
      </Col>
      <Col>
        <Nav className="justify-content-end">
          <NavDropdown title={<Bell />} className="notifications">
            {notifications.map((notification, index) => (
              <div key={notification.notificationId}>
                {index > 0 && <hr className="my-0"></hr>}
                <NavDropdown.Item
                  className="nav-link ToolbarLink text-secondary"
                  onClick={() => {
                    if (
                      notification.notificationStatus ===
                      NotificationStatuses.UNREAD
                    )
                      readNotification(notification);
                  }}
                >
                  <div>
                    {notification.notificationStatus ===
                      NotificationStatuses.UNREAD && (
                      <span className="text-primary">
                        <b>*</b>
                      </span>
                    )}
                    <span>
                      Your application for {notification.internshipOfferName}{" "}
                      at&nbsp;
                      {notification.companyName} in now&nbsp;
                      <b>
                        {
                          ApplicationStatusesToString[
                            notification.applicationStatus
                          ]
                        }
                      </b>
                      .
                    </span>
                  </div>
                  <div className="text-end">
                    <small>
                      {DateTime.fromISO(
                        notification.notificationTimestamp
                      ).toRelative()}
                    </small>
                  </div>
                </NavDropdown.Item>
              </div>
            ))}
            {notifications.length === 0 && (
              <div className="text-muted">No new notifications</div>
            )}
          </NavDropdown>
          <NavDropdown title={<Person />}>
            <NavDropdown.Item
              className="nav-link ToolbarLink"
              href="/student/documents"
            >
              My Documents
            </NavDropdown.Item>
            <NavDropdown.Item
              className="nav-link ToolbarLink"
              href="/student/applications"
            >
              My Applications
            </NavDropdown.Item>
            <NavDropdown.Item
              className="nav-link ToolbarLink"
              onClick={() => logout()}
            >
              Logout
            </NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Col>
    </Row>
  );
};
