import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../auth/components/AuthContextProvider";
import { Navigate } from "react-router-dom";
import { Card, CardBody, Container, Form } from "react-bootstrap";
import { StudentToolbar } from "./StudentToolbar";
import { Download } from "react-bootstrap-icons";
import { UploadDocumentApi } from "../api/UploadDocument";
import { handleError } from "../../utils/ErrorToast";
import { handleSuccess } from "../../utils/SuccessToast";
import { GetDocumentByIdApi, GetMyDocumentsApi } from "../api/GetMyDocuments";
import FileSaver from "file-saver";

export const MyDocumentsComponent = () => {
  const { auth, logout } = useContext(AuthContext);
  const [file, setFile] = useState({ name: "", file: null });
  const [documents, setDocuments] = useState([]);

  const getDocuments = () => {
    GetMyDocumentsApi(auth)
      .catch((err) => {
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((r) => {
        if (r) {
          if (r.status === 200) {
            setDocuments(r.data);
          }
        }
      });
  };

  useEffect(() => {
    getDocuments();
  }, []);

  if (!auth) {
    return <Navigate to={"/login"} />;
  }

  const downloadFile = (document) => {
    GetDocumentByIdApi(document.documentId, auth)
      .catch((err) => {
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((response) => {
        if (response) {
          if (response.status === 200) {
            FileSaver.saveAs(response.data, document.documentName);
          }
        }
      });
  };

  const uploadDocument = (e) => {
    e.preventDefault();
    if (!file.file) {
      return;
    }
    const formData = new FormData();
    formData.append("document", file.file);

    UploadDocumentApi(formData, auth)
      .catch((err) => {
        handleError("File could not be uploaded");
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((r) => {
        if (r) {
          if (r.status === 200) {
            handleSuccess("File uploaded successfully");
            setDocuments([...documents, r.data]);
            var form = document.getElementById("form");
            form.reset();
            setFile({ name: "", file: null });
          }
        }
      });
  };

  return (
    <Container fluid className="h-100">
      <StudentToolbar />
      <div className="page-content container d-flex flex-column justify-content-center text-center w-50">
        <Card className="container h-75">
          <CardBody className="formBody d-flex flex-column justify-content-between">
            <div>
              <h5 className="my-4">My documents</h5>
              {documents.map((doc, index) => (
                <>
                  {index > 0 && <hr className="my-2"></hr>}
                  <div
                    className="d-flex justify-content-between px-2 align-items-center"
                    key={doc.documentId}
                  >
                    {doc.documentName}
                    <button
                      className="btn"
                      onClick={() => {
                        downloadFile(doc);
                      }}
                    >
                      <Download />
                    </button>
                  </div>
                </>
              ))}
            </div>
            <Form
              id="form"
              onSubmit={uploadDocument}
              className="d-flex gap-5 my-4"
            >
              <div className="formRow">
                <Form.Control
                  type="file"
                  onChange={(event) => {
                    setFile({
                      name: event.target.files[0].name,
                      file: event.target.files[0],
                    });
                  }}
                />
              </div>
              <div className="formRow">
                <Form.Control
                  type="submit"
                  value="Upload"
                  className="btn btn-primary"
                />
              </div>
            </Form>
          </CardBody>
        </Card>
      </div>
    </Container>
  );
};
