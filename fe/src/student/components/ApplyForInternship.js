import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../auth/components/AuthContextProvider";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import { Card, CardBody, Container } from "react-bootstrap";
import { StudentToolbar } from "./StudentToolbar";
import { GetInternshipOfferApi } from "../api/GetInternshipOffer";
import { GetDocumentByIdApi, GetMyDocumentsApi } from "../api/GetMyDocuments";
import FileSaver from "file-saver";
import { ApplyToInternshipApi } from "../api/ApplyToInternship";
import { handleSuccess } from "../../utils/SuccessToast";
import { handleError } from "../../utils/ErrorToast";

export const ApplyForInternship = () => {
  const { auth, logout } = useContext(AuthContext);
  const { id } = useParams();
  const navigate = useNavigate();
  const [offer, setOffer] = useState(null);
  const [documents, setDocuments] = useState([]);
  const [selectedDocuments, setSelectedDocuments] = useState([]);

  const getDocuments = () => {
    GetMyDocumentsApi(auth)
      .catch((err) => {
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((r) => {
        if (r) {
          if (r.status === 200) {
            setDocuments(r.data);
          }
        }
      });
  };

  useEffect(() => {
    if (id) {
      GetInternshipOfferApi(id, auth)
        .catch((err) => {
          if (err.response) {
            if (err.response.status === 401 || err.response.status === 403) {
              logout();
            }
          }
        })
        .then((r) => {
          if (r) {
            if (r.status === 200) {
              setOffer(r.data);
            }
          }
        });
      getDocuments();
    }
  }, [auth, logout, id]);

  if (!auth) {
    return <Navigate to={"/login"} />;
  }

  const changeSelected = (id) => {
    if (!selectedDocuments.includes(id)) {
      setSelectedDocuments([...selectedDocuments, id]);
      return;
    }
    let newDocList = [...selectedDocuments];
    const index = newDocList.indexOf(id);
    newDocList.splice(index, 1);
    setSelectedDocuments(newDocList);
  };

  const downloadFile = (document) => {
    GetDocumentByIdApi(document.documentId, auth)
      .catch((err) => {
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((response) => {
        if (response) {
          if (response.status === 200) {
            FileSaver.saveAs(response.data, document.documentName);
          }
        }
      });
  };

  const sendApplication = () => {
    if (selectedDocuments.length === 0) {
      handleError("You must select at least one document!");
      return;
    }
    ApplyToInternshipApi({ documentIds: selectedDocuments, offerId: id }, auth)
      .catch((err) => {
        if (err.response) {
          handleError("Application could not be sent!");
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((response) => {
        if (response) {
          if (response.status === 200) {
            handleSuccess("Intenship application sent successfully");
            navigate("/student/applications");
          }
        }
      });
  };

  return (
    <Container fluid className="h-100">
      <StudentToolbar />
      <div className="page-content container d-flex flex-column justify-content-center text-center w-50">
        <Card className="container h-75">
          <CardBody className="formBody d-flex flex-column justify-content-between">
            <div>
              <h5 className="my-4">
                Choose documents for applying to <b>{offer?.name}</b>
              </h5>
              {documents.map((doc, index) => (
                <div
                  className="d-flex px-2 align-items-center gap-2"
                  key={doc.documentId}
                >
                  <input
                    className="form-check-input"
                    type="checkbox"
                    value={doc.documentId}
                    id={index}
                    checked={selectedDocuments.includes(doc.documentId)}
                    onChange={() => changeSelected(doc.documentId)}
                  />
                  <span
                    className="document-link"
                    onClick={() => {
                      downloadFile(doc);
                    }}
                  >
                    {doc.documentName}
                  </span>
                </div>
              ))}
            </div>
            <div className="formRow">
              <button
                type="button"
                className="btn btn-primary"
                onClick={sendApplication}
              >
                Send Application
              </button>
            </div>
          </CardBody>
        </Card>
      </div>
    </Container>
  );
};
