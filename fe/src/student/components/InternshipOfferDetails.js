import { Container } from "react-bootstrap";
import { useContext, useEffect, useState } from "react";
import "react-datepicker/dist/react-datepicker.css";
import { StudentToolbar } from "./StudentToolbar";
import { AuthContext } from "../../auth/components/AuthContextProvider";
import { useParams, useNavigate } from "react-router-dom";
import {
  ApplicationStatuses,
  GetInternshipOfferApi,
} from "../api/GetInternshipOffer";
import { CheckCircleFill, Download, XCircleFill } from "react-bootstrap-icons";
import { GetDocumentByIdApi } from "../api/GetMyDocuments";
import FileSaver from "file-saver";

export const InternshipOfferDetails = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [offer, setOffer] = useState(null);
  const { auth, logout } = useContext(AuthContext);

  useEffect(() => {
    if (id) {
      GetInternshipOfferApi(id, auth)
        .catch((err) => {
          if (err.response) {
            if (err.response.status === 401 || err.response.status === 403) {
              logout();
            }
          }
        })
        .then((r) => {
          if (r) {
            if (r.status === 200) {
              setOffer(r.data);
            }
          }
        });
    }
  }, [auth, logout, id]);

  const renderButton = (status) => {
    switch (status) {
      case ApplicationStatuses.AWAITING_FOR_REVIEW:
        return <span className="badge bg-warning">Awaiting For Review</span>;
      case ApplicationStatuses.UNDER_REVIEW:
        return <span className="badge bg-info">Under Review</span>;
      case ApplicationStatuses.ACCEPTED:
        return <span className="badge bg-success">Accepted</span>;
      case ApplicationStatuses.REJECTED:
        return <span className="badge bg-danger">Rejected</span>;
      default:
        return (
          <button
            className="btn btn-primary"
            type="button"
            onClick={() => {
              navigate(`/offer/${id}/apply`);
            }}
          >
            Apply
          </button>
        );
    }
  };

  const downloadFile = (document) => {
    GetDocumentByIdApi(document.documentId, auth)
      .catch((err) => {
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((response) => {
        if (response) {
          if (response.status === 200) {
            FileSaver.saveAs(response.data, document.documentName);
          }
        }
      });
  };

  return (
    <Container fluid>
      <StudentToolbar />
      {offer && (
        <div className="m-5 pt-5">
          <div className="d-flex justify-content-between">
            <h2>{offer.name}</h2>
            {renderButton(offer.status)}
          </div>
          <div className="my-4">
            <div className="my-2">
              <b>Company:</b>&nbsp;
              {offer.publishedBy}
            </div>
            <div className="my-2">
              <b>Domain:</b>&nbsp;
              {offer.domainOfWork}
            </div>
            <div className="my-2">
              <b>Location:</b>&nbsp;
              {offer.location?.city}, {offer.location?.country}
            </div>
            <div className="my-2">
              <b>Is paid?</b>&nbsp;
              {offer.paid ? (
                <CheckCircleFill className="text-success" />
              ) : (
                <XCircleFill className="text-danger" />
              )}
            </div>
            {offer.paid && (
              <div className="my-2">
                <b>Salary:</b>&nbsp;
                {offer.salary}
              </div>
            )}
            <div className="my-2">
              <b>Starting date:</b>&nbsp;
              {offer.startsOn}
            </div>
            <div className="my-2">
              <b>Duration:</b>&nbsp;
              {offer.durationInMonths} months
            </div>
            <div className="my-2">
              <b>Description:</b>&nbsp;
              <div>{offer.description}</div>
            </div>
            <div className="my-2">
              <b>Required skills:</b>&nbsp;
              {offer.requiredSkills?.join(", ")}
            </div>
            <div className="my-2">
              <b>Documents needed for applying:</b>&nbsp;
              {offer.requiredDocuments?.join(", ")}
            </div>
          </div>
          <div className="my-2">
            {offer.uploadedDocuments && offer.uploadedDocuments.length > 0 && (
              <b>Application documents:</b>
            )}
            {offer.uploadedDocuments &&
              offer.uploadedDocuments.length > 0 &&
              offer.uploadedDocuments.map((doc, index) => (
                <>
                  {index > 0 && <hr className="col-md-3 my-0" />}
                  <div
                    className="d-flex justify-content-between  align-items-center col-md-3"
                    key={doc.documentId}
                  >
                    {doc.documentName}
                    <button
                      className="btn"
                      onClick={() => {
                        downloadFile(doc);
                      }}
                    >
                      <Download />
                    </button>
                  </div>
                </>
              ))}
          </div>
        </div>
      )}
    </Container>
  );
};
