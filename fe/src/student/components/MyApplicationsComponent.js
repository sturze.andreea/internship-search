import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../auth/components/AuthContextProvider";
import { Navigate, useNavigate } from "react-router-dom";
import { Card, CardBody, Container } from "react-bootstrap";
import { StudentToolbar } from "./StudentToolbar";
import { Search } from "react-bootstrap-icons";
import { GetMyApplicationsApi } from "../api/GetMyApplications";
import { ApplicationStatuses } from "../api/GetInternshipOffer";

export const MyApplicationsComponent = () => {
  const { auth, logout } = useContext(AuthContext);
  const navigate = useNavigate();
  const [applications, setApplications] = useState([]);

  const getApplications = () => {
    GetMyApplicationsApi(auth)
      .catch((err) => {
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((r) => {
        if (r) {
          if (r.status === 200) {
            setApplications(r.data);
          }
        }
      });
  };

  useEffect(() => {
    getApplications();
  }, []);

  if (!auth) {
    return <Navigate to={"/login"} />;
  }

  const renderStatus = (status) => {
    switch (status) {
      case ApplicationStatuses.AWAITING_FOR_REVIEW:
        return <span className="text-warning">Awaiting For Review</span>;
      case ApplicationStatuses.UNDER_REVIEW:
        return <span className="text-info">Under Review</span>;
      case ApplicationStatuses.ACCEPTED:
        return <span className="text-success">Accepted</span>;
      case ApplicationStatuses.REJECTED:
        return <span className="text-danger">Rejected</span>;
      default:
        return null;
    }
  };

  return (
    <Container fluid className="h-100">
      <StudentToolbar />
      <div className="page-content container d-flex flex-column justify-content-center text-center w-50">
        <Card className="container h-75">
          <CardBody className="formBody d-flex flex-column justify-content-between">
            <div>
              <h5 className="my-4">My applications</h5>
              {applications.map((application, index) => (
                <div key={application.id}>
                  {index > 0 && <hr className="my-2"></hr>}
                  <div className="d-flex justify-content-between px-2 align-items-center">
                    {application.offerName}
                    {renderStatus(application.status)}
                    <div>
                      <button
                        className="btn"
                        onClick={() => {
                          navigate(`/student/search/${application.offerId}`);
                        }}
                      >
                        <Search />
                      </button>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </CardBody>
        </Card>
      </div>
    </Container>
  );
};
