import { Container, Dropdown, Form } from "react-bootstrap";
import DatePicker from "react-datepicker";
import { useContext, useEffect, useState } from "react";
import "react-datepicker/dist/react-datepicker.css";
import { StudentToolbar } from "./StudentToolbar";
import { Formik, Form as FormFormik, Field } from "formik";
import {
  OfferPaymentType,
  OfferPaymentTypeList,
  QueryInternshipOffersApi,
} from "../api/QueryinternshipOffers";
import { AuthContext } from "../../auth/components/AuthContextProvider";
import { NavLink } from "react-router-dom";

const convertToLocalDateStr = (date) => {
  const indexOfT = date.toISOString().indexOf("T");
  return date.toISOString().substr(0, indexOfT);
};

export const SearchInternshipOffers = () => {
  const FILTER_OFFERS_INITIAL_STATE = {
    filter: "",
    domain: "",
    startsAfter: null,
    duration: "",
    location: "",
    paid: OfferPaymentType.ALL,
  };

  const [filterState, setFilterState] = useState(FILTER_OFFERS_INITIAL_STATE);
  const [offers, setOffers] = useState([]);
  const { auth, logout } = useContext(AuthContext);

  useEffect(() => {
    QueryInternshipOffersApi({}, auth)
      .catch((err) => {
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((r) => {
        if (r) {
          if (r.status === 200) {
            setOffers(r.data);
          }
        }
      });
  }, [auth, logout]);

  const handleFilter = (filters) => {
    const sentFilters = { ...filters };
    if (filters.startsAfter) {
      sentFilters.startsAfter = convertToLocalDateStr(filters.startsAfter);
    } else {
      sentFilters.startsAfter = null;
    }

    QueryInternshipOffersApi(sentFilters, auth)
      .catch((err) => {
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((r) => {
        if (r) {
          if (r.status === 200) {
            setOffers(r.data);
          }
        }
      });
  };

  const handleReset = () => {
    setFilterState(FILTER_OFFERS_INITIAL_STATE);
    QueryInternshipOffersApi({}, auth)
      .catch((err) => {
        if (err.response) {
          if (err.response.status === 401 || err.response.status === 403) {
            logout();
          }
        }
      })
      .then((r) => {
        if (r) {
          if (r.status === 200) {
            setOffers(r.data);
          }
        }
      });
  };

  return (
    <Container fluid>
      <StudentToolbar />

      <div className="m-5">
        <Formik
          initialValues={filterState}
          onSubmit={handleFilter}
          onReset={handleReset}
        >
          <FormFormik className="d-flex flex-column">
            <div>
              <Field id="filter" name="filter">
                {({ field }) => (
                  <Form.Control
                    type="text"
                    {...field}
                    placeholder="Filter..."
                  />
                )}
              </Field>
            </div>
            <div className="d-flex flex-row my-3 gap-2">
              <Field id="domain" name="domain">
                {({ field }) => (
                  <Form.Control type="text" {...field} placeholder="Domain" />
                )}
              </Field>
              <Field id="location" name="location">
                {({ field }) => (
                  <Form.Control type="text" {...field} placeholder="Location" />
                )}
              </Field>
              <Field id="duration" name="duration">
                {({ field }) => (
                  <Form.Control
                    type="number"
                    {...field}
                    placeholder="Months"
                    min="1"
                  />
                )}
              </Field>
              <Field id="startsAfter" name="startsAfter">
                {({ form, field }) => (
                  <div className="col-md-2">
                    <DatePicker
                      className="form-control"
                      selected={field.value}
                      {...field}
                      onChange={(value) =>
                        form.setFieldValue("startsAfter", value)
                      }
                      id="startsAfter"
                      placeholderText="Starting after"
                    />
                  </div>
                )}
              </Field>
              <Field>
                {({ form, field }) => (
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="paid">
                      {"Payment Type: " + filterState.paid}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {OfferPaymentTypeList.map((option, index) => (
                        <Dropdown.Item
                          key={"requiredSkillsOption" + index}
                          onClick={(e) => {
                            form.setFieldValue("paid", option);
                            setFilterState({ ...filterState, paid: option });
                          }}
                          active={filterState.paid === option}
                          defaultValue={OfferPaymentType.ALL}
                        >
                          {option}
                        </Dropdown.Item>
                      ))}
                    </Dropdown.Menu>
                  </Dropdown>
                )}
              </Field>
            </div>

            <div>
              <button className="btn btn-primary" type="submit">
                Filter
              </button>
              <button className="btn btn-danger ms-3" type="reset">
                Clear
              </button>
            </div>
          </FormFormik>
        </Formik>
        <div className="mt-4">
          {offers.map((offer, index) => (
            <div key={offer.id}>
              {index > 0 && <hr></hr>}
              <NavLink
                className="text-decoration-none text-reset"
                to={`/student/search/${offer.id}`}
              >
                <h4>
                  {offer.name} ({offer.domainOfWork})
                </h4>
              </NavLink>
              <p className="text-truncate">{offer.description}</p>
            </div>
          ))}
        </div>
      </div>
    </Container>
  );
};
