import axios from "axios";
import { GetAuthorizationHeader } from "../../auth/utils/GetAuthorizationHeader";

export const QueryInternshipOffersApi = (filters, auth) => {
  return axios.get(
    `http://localhost:8080/be/api/offers?${
      filters.filter ? `filter=${filters.filter}&` : ""
    }${filters.domain ? `domain=${filters.domain}&` : ""}${
      filters.location ? `location=${filters.location}&` : ""
    }${filters.duration ? `duration=${filters.duration}&` : ""}${
      filters.startsAfter ? `startsAfter=${filters.startsAfter}&` : ""
    }${filters.paid ? `paid=${filters.paid}` : ""}`,

    GetAuthorizationHeader(auth)
  );
};
export const OfferPaymentType = {
  ALL: "all",
  PAID: "paid",
  UNPAID: "unpaid",
};
export const OfferPaymentTypeList = ["all", "paid", "unpaid"];
