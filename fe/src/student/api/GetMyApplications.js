import axios from "axios";
import { GetAuthorizationHeader } from "../../auth/utils/GetAuthorizationHeader";

export const GetMyApplicationsApi = (auth) => {
  return axios.get(
    `http://localhost:8080/be/api/applications`,
    GetAuthorizationHeader(auth)
  );
};
