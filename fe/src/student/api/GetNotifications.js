import axios from "axios";
import { GetAuthorizationHeader } from "../../auth/utils/GetAuthorizationHeader";

export const GetNotificationsApi = (auth) => {
  return axios.get(
    `http://localhost:8080/be/api/notifications`,
    GetAuthorizationHeader(auth)
  );
};
export const ReadNotificationApi = (id, auth) => {
  return axios.get(
    `http://localhost:8080/be/api/notification/${id}`,
    GetAuthorizationHeader(auth)
  );
};

export const NotificationStatuses = {
  UNREAD: "UNREAD",
  READ: "READ",
};
