import axios from "axios";
import { GetAuthorizationHeader } from "../../auth/utils/GetAuthorizationHeader";

export const UploadDocumentApi = (doc, auth) => {
  return axios.post(
    "http://localhost:8080/be/api/document",
    doc,
    GetAuthorizationHeader(auth)
  );
};
