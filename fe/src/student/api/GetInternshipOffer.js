import axios from "axios";
import { GetAuthorizationHeader } from "../../auth/utils/GetAuthorizationHeader";

export const GetInternshipOfferApi = (id, auth) => {
  return axios.get(
    `http://localhost:8080/be/api/offer/${id}`,
    GetAuthorizationHeader(auth)
  );
};

export const ApplicationStatuses = {
  AWAITING_FOR_REVIEW: "AWAITING_FOR_REVIEW",
  UNDER_REVIEW: "UNDER_REVIEW",
  ACCEPTED: "ACCEPTED",
  REJECTED: "REJECTED",
};

export const ApplicationStatusesToString = {
  AWAITING_FOR_REVIEW: "awaiting for review",
  UNDER_REVIEW: "under review",
  ACCEPTED: "accepted",
  REJECTED: "rejected",
};
