import axios from "axios";
import { GetAuthorizationHeader } from "../../auth/utils/GetAuthorizationHeader";

export const ApplyToInternshipApi = (application, auth) => {
  return axios.post(
    "http://localhost:8080/be/api/application",
    application,
    GetAuthorizationHeader(auth)
  );
};