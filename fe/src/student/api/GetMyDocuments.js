import axios from "axios";
import { GetAuthorizationHeader } from "../../auth/utils/GetAuthorizationHeader";

export const GetMyDocumentsApi = (auth) => {
  return axios.get(
    `http://localhost:8080/be/api/documents`,
    GetAuthorizationHeader(auth)
  );
};

export const GetDocumentByIdApi = (id, auth) => {
  return axios.get(`http://localhost:8080/be/api/document/${id}`, {
    ...GetAuthorizationHeader(auth),
    responseType: "blob",
  });
};
