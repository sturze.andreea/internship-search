import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import { PublishInternshipOfferComponent } from "./company/components/PublishInternshipOfferComponent";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { SearchInternshipOffers } from "./student/components/SearchInternshipOffers";
import { AuthContextProvider } from "./auth/components/AuthContextProvider";
import { ProtectedRoute } from "./auth/components/ProtectedRoute";
import { UserRoles } from "./auth/components/UserRoles";
import { LoginComponent } from "./auth/components/LoginComponent";
import { RegisterComponent } from "./auth/components/RegisterComponent";
import { InternshipOfferDetails } from "./student/components/InternshipOfferDetails";
import { RegisterStudentComponent } from "./auth/components/RegisterStudentComponent";
import { RegisterCompanyComponent } from "./auth/components/RegisterCompanyComponent";
import { ToastContainer } from "react-toastify";
import { MyDocumentsComponent } from "./student/components/MyDocumentsComponent";
import { MyApplicationsComponent } from "./student/components/MyApplicationsComponent";
import { ApplyForInternship } from "./student/components/ApplyForInternship";

function App() {
  return (
    <AuthContextProvider>
      <BrowserRouter>
        <Routes>
          <Route
            path="/login"
            element={
              <ProtectedRoute acceptedRole={UserRoles.ANY}>
                <LoginComponent />
              </ProtectedRoute>
            }
          ></Route>
          <Route
            path="/"
            exact
            element={
              <ProtectedRoute acceptedRole={UserRoles.ANY}>
                <LoginComponent />
              </ProtectedRoute>
            }
          ></Route>
          <Route
            path="/register/student"
            element={
              <ProtectedRoute acceptedRole={UserRoles.ANY}>
                <RegisterStudentComponent />
              </ProtectedRoute>
            }
          ></Route>
          <Route
            path="/register/company"
            element={
              <ProtectedRoute acceptedRole={UserRoles.ANY}>
                <RegisterCompanyComponent />
              </ProtectedRoute>
            }
          ></Route>
          <Route
            path="/register"
            element={
              <ProtectedRoute acceptedRole={UserRoles.ANY}>
                <RegisterComponent />
              </ProtectedRoute>
            }
          ></Route>
          <Route
            path="/company/offer/publish"
            element={
              <ProtectedRoute acceptedRole={UserRoles.COMPANY}>
                <PublishInternshipOfferComponent />
              </ProtectedRoute>
            }
          ></Route>
          <Route
            path="/student/search/:id"
            element={
              <ProtectedRoute acceptedRole={UserRoles.STUDENT}>
                <InternshipOfferDetails />
              </ProtectedRoute>
            }
          ></Route>
          <Route
            path="/student/search"
            element={
              <ProtectedRoute acceptedRole={UserRoles.STUDENT}>
                <SearchInternshipOffers />
              </ProtectedRoute>
            }
          ></Route>
          <Route
            path="/student/documents"
            element={
              <ProtectedRoute acceptedRole={UserRoles.STUDENT}>
                <MyDocumentsComponent />
              </ProtectedRoute>
            }
          ></Route>
          <Route
            path="/student/applications"
            element={
              <ProtectedRoute acceptedRole={UserRoles.STUDENT}>
                <MyApplicationsComponent />
              </ProtectedRoute>
            }
          ></Route>
          <Route
            path="/offer/:id/apply"
            element={
              <ProtectedRoute acceptedRole={UserRoles.STUDENT}>
                <ApplyForInternship />
              </ProtectedRoute>
            }
          ></Route>
        </Routes>
      </BrowserRouter>
      <ToastContainer />
    </AuthContextProvider>
  );
}

export default App;
