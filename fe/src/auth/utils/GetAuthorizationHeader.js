export const GetAuthorizationHeader = (auth) => {
  return {
    headers: {
      Authorization: auth.token,
    },
  };
};
