import { Card, CardBody, Col, Form, Row } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { useContext, useRef, useState } from "react";
import { AuthContext } from "./AuthContextProvider";
import { handleError } from "../../utils/ErrorToast";
import { getRedirectBasedOnAuthRole } from "../../utils/redirectBasedOnRole";

export const LoginComponent = () => {
  const email = useRef("");
  const password = useRef("");
  const { auth, login } = useContext(AuthContext);
  const [usernameNotFound, setUsernameNotFound] = useState(false);
  const [invalidPassword, setInvalidPassword] = useState(false);
  const [usernameRequired, setUsernameRequired] = useState(false);
  const [passwordRequired, setPasswordRequired] = useState(false);

  const handleClickOnSubmitBtn = async (e) => {
    e.preventDefault();
    let loginFailed = false;
    if (!email.current.value || !password.current.value) {
      e.preventDefault();
      e.stopPropagation();
      !email.current.value && setUsernameRequired(true);
      !password.current.value && setPasswordRequired(true);
      return;
    } else {
      setUsernameRequired(false);
      setPasswordRequired(false);
    }
    await login({
      email: email.current.value,
      password: password.current.value,
    })
      .catch((err) => {
        loginFailed = true;
        if (err) {
          handleError("Incorrect email or password!");
          if (err.response) {
            if (err.response.status === 404) {
              setUsernameNotFound(true);
              setInvalidPassword(false);
            } else if (err.response.status === 401) {
              setInvalidPassword(true);
              setUsernameNotFound(false);
            }
          }
        }
      })
      .then(() => {
        if (!loginFailed) {
          return getRedirectBasedOnAuthRole(auth);
        }
      });
  };

  if (auth) {
    return getRedirectBasedOnAuthRole(auth);
  }

  return (
    <div className="container d-flex flex-column justify-content-center text-center authForm w-50">
      <h2 className="mb-4">
        <b>Login</b>
      </h2>

      <div className="d-flex">
        <Card className="container">
          <CardBody className="formBody">
            <Form>
              <Row className="formRow">
                <Col className="col-md-10 mx-auto">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="text"
                    id="emailInput"
                    ref={email}
                    required
                  />
                  {usernameNotFound && (
                    <p className="error">Username not found</p>
                  )}
                  {usernameRequired && (
                    <span className="error">Username is required</span>
                  )}
                </Col>
              </Row>
              <Row className="formRow">
                <Col className="col-md-10 mx-auto">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    required
                    type="password"
                    id="passwordInput"
                    ref={password}
                  />
                  {invalidPassword && <p className="error">Invalid password</p>}
                  {passwordRequired && (
                    <span className="error">Password is required</span>
                  )}
                </Col>
              </Row>
              <Row className="formRow">
                <Col className="col-md-4 mx-auto">
                  <Form.Control
                    id="publishOfferSubmit"
                    type="submit"
                    value="Log In"
                    className="btn btn-primary"
                    onClick={handleClickOnSubmitBtn}
                  />
                </Col>
              </Row>
              <Row className="formRow">
                <Col className=" mx-auto text-center">
                  Don't have an account? Register&nbsp;
                  <NavLink className="text-primary" to={"/register"}>
                    here
                  </NavLink>
                  .
                </Col>
              </Row>
            </Form>
          </CardBody>
        </Card>
      </div>
    </div>
  );
};
