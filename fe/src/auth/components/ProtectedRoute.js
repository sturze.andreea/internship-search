import { useContext } from "react";
import { AuthContext } from "./AuthContextProvider";
import { Navigate } from "react-router-dom";
import { UserRoles } from "./UserRoles";

export const ProtectedRoute = ({ children, acceptedRole }) => {
  const { auth } = useContext(AuthContext);
  if (acceptedRole === UserRoles.ANY) {
    return children;
  } else if (auth) {
    if (auth.role === acceptedRole) {
      return children;
    }
  }
  return <Navigate to={"/login"} />;
};
