import student from "../../assets/student.svg";
import company from "../../assets/company.svg";
import { NavLink, useNavigate } from "react-router-dom";

export const RegisterComponent = () => {
  const navigate = useNavigate();
  return (
    <div className="h-100 container d-flex flex-column justify-content-center text-center">
      <h1 className="text-center mb-5"> Register as</h1>
      <div className="d-flex flex-row justify-content-center gap-5">
        <div
          className="register-card"
          onClick={() => {
            navigate("/register/student");
          }}
        >
          <img src={student} alt="student" />
          <h2 className="text-primary">Student</h2>
        </div>
        <div
          className="register-card"
          onClick={() => {
            navigate("/register/company");
          }}
        >
          <img src={company} alt="company" />
          <h2 className="text-primary">Company</h2>
        </div>
      </div>
      <div className="mt-4">
        <div className="mx-auto text-center">
          Already have an account?&nbsp;
          <NavLink className="text-primary" to={"/login"}>
            Login
          </NavLink>
          .
        </div>
      </div>
    </div>
  );
};
