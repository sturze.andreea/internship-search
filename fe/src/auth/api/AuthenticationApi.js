import axios from "axios";

export const LoginApi = ({ email, password }) => {
  return axios.post("http://localhost:8080/be/api/account/login", {
    email,
    password,
  });
};

export const RegisterStudentApi = ({
  firstName,
  lastName,
  email,
  password,
}) => {
  return axios.post("http://localhost:8080/be/api/student/profile/register", {
    firstName,
    lastName,
    email,
    password,
  });
};

export const RegisterCompanyApi = ({
  name,
  location,
  description,
  email,
  password,
}) => {
  return axios.post("http://localhost:8080/be/api/company/profile/register", {
    name,
    location,
    description,
    email,
    password,
  });
};
